
###### initialiser un projet git
Créer le projet sur gitlab, y déclarer la clé ssh présente dans :
`~/.ssh/id_rsa (cle publique)`

###### En local
```
git remote add origin
git@gitlab.mim.ovh:edouard.lebourgeois/meteor_contest_visio.git
git pull origin master
git push --set-upstream origin master
```

###### A la racine du projet
pour modifier les urls git entre repository
`.git/config`

###### Les commandes git
|commande|description|
|---|---|
|`git branch <name>`|créer une branch|
|`git branch`|montre les branch|
|`git branch -d <nom_de_la_branche>`|supprime une branch locale|
|`git branch -D <nom_de_la_branche>`|supprime les branches locales et distantes|
|`git branch -v`|branch list with there last log|
|`git branch -a`|list of branchs on local and distant including|
|`git cat-file -p HEAD`|affiche le contenu du commit HEAD|
|`git checkout -b new_branch`|create a new branch && go to that branch|
|`git checkout ba26558a`|follow by first letters of key log ( or complete key ) to go back on a previous log.|
|`git checkout @{-1} / git co -`|revenir sur la branch précédente|
|`git commit`|faire un log, près à être pousser|
|`git commit --amend`|rename last commit|
|`git config --global core.editor "vi"`|change default editor when commit|
|`git diff`|montre les modifs dans staged|
|`git diff --staged`|montre les modifs dans commit|
|`git diff --cached`|#|
|`git diff 2e91 7ee6 index.js`|compare deux fichiers dans deux commits différents|
|`git fetch --all -prune`|répandre un changement sur toutes les autres machines suite à une suppression de branche|
|`git fsck --full`|#|
|`git fsck --unreachable`|#|
|`git fsck --strict`|#|
|`git fetch`|observe tous les changements qui ont été fait en distant|
|`git init`|initialise un dépot git|
|`git init --bare`|Créer un depot git central pour éviter le peer to peer|
|`git log`|liste des commits|
|`git log -p`|liste des commits ainsi que le detail des changements par commit|
|`git log --oneline`|#|
|`git log --graph`|#|
|`git log --summary`|#|
|`git log develop..HEAD`|#|
|`git log HEAD..@{u}`|#|
|`git log --stat`|affiche le nombre de changement|
|`git merge <nom de la branch>`|se placer sur la branch vers la quelle on veut merger (ne pas sciller la branche sur la-quelle on est assis )|
|`git push origin new_branch`|origin = depot depuis lequelle j'ai cloné. new_branch = nom de la branch donné dans le depot principal|
|`git push origin :<nom_de_la_branche>`|supprime la branche distante ( si j'ai utilisé la commande git clone )|
|`git push origin --delete <nom_de_la_branche>`|idem|
|`git push -u origin new_branch`|-u allow to ancor link between origin and branch => thereafter git push will be enough|
|`git rebase -i HEAD~n`|to rename the last n commit ( remove "pick" to "reword" in front of the commit you want to change / "git push --force" ATTENTION )|
|`git rebase -i <numero du commit>`|replace first pick by r and the others by f to transform all commits into one|
|`git rebase -i develop`|#|
|`git rebase --abort`|abandonner le rebase|
|`git remote prune origin`|supprime sur le local les branches qui n'existent plus sur le distant|
|`git reset --soft develop`|#|
|`git reset --hard develop`|( en cas de conflit dans l'historique de git )|
|`git reset --mixed develop`|#|
|`git rev-parse HEAD`|#|
|`git status`|status des fichiers suivits et non suivits|
|`git show`|montre les modifs du dernier commit|
|`git stash`|save last modif in a temporary memory out of the branch wich allow to back to a previous log despite of the new modifications|
|`git stash pop`|récupère les fichiers non commiter depuis le stash|
|`git zless .git/objects/ea/<numero de commit>`|#|

###### Git flow
|commande|description|
|---|---|
|`git flow feature start \<toto>`|créer une branch|
|`git flow feature finish \<toto>`|merger une branch|

###### Annuler git flow init
|commande|description|
|---|---|
|`git config --remove-section "gitflow.path"`|#|
|`git config --remove-section "gitflow.prefix"`|#|
|`git config --remove-section "gitflow.branch"`|supprime des éléments dans le fichier config de git|

###### Supprimer un répertoire du dépo distant après l'avoir ajouter au .gitignore
```
git rm -r --cached some-directory
git commit -m 'Remove the now ignored directory "some-directory"'
git push origin master
```
