
----
localisation des snippets
`~/.config/VSCodium/User/snippets`

---
###### MY EXTENSIONS
---
- Atom Keymap
- Bracket Pair Colorizer 2
- File-Icons
- Five-Server
- French Language Pack Visual Studio Code
- MongoDB for VS Code
- Auto Rename Tag Clone
- Prettier - Code Formatter
- DotENV
