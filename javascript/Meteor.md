
Install React npm dependencies
`meteor npm install --save react react-dom`

ajouter des packets
`meteor add <package>`

supprimer des packets
`meteor remove <package>`

list les packets installés
`meteor list`

donne accès à la db
`meteor mongo`

affiche une console dans laquelle on peut voir toutes les méthodes disponibles pour meteor
`meteor shell`

pour "seeder" les utilisateurs, on peut créer un script.sh qui va prendre en compte un script.js
`meteor mongo < script.js`

Dans le script.js on peut lui passer toutes les commandes de mongo shell qu'on veut, en l'occurence :
`db.users.insert({ _id : "NaTDjftnKS5cN4zSX", createdAt : new Date(), services : { password : { bcrypt : "$2a$10$1MaU5IbEknr6VvCTwFptQOMQnHK/xzryMFnof58kJoRMdMmmCtCAe" }, resume : { loginTokens : [ { when : new Date(), hashedToken : "6x5rP5FptJ77nPE+wnPpWIQB4ohAq7fVxpSdxj8bgHM=" } ] } }, username : "ede" });`

installation de moment.js
`meteor add momentjs:moment`

pour modifier la langue
`meteor add jeeeyul:moment-with-langs`

How to build
`sudo apt install mongodb-clients`

clone new project wihtout node modules and run:
`npm install --production`

create new repository outside of the porject like so:
`md ../bundle`
`meteor build ../bundle`
`cd ../bundle`
`tar xvf mon_projet_meteor.tar.gz`
`cd bundle/programs/server`
`npm install`

install mongodb with docker : (l'option - - name définie le nom du conteneur, -d permet de rendre la main, -p permet de mapper le port du conteneur avec le port du serveur)
`sudo docker run --name mongo -d -p 27017:27017 mongo`

if you use docker :
```
sudo docker run -d \
	-e ROOT_URL=http://localhost \
	-e MONGO_URL=mongodb://localhost:27017/myapps \
	-v /mybundle_dir:/home/edouard/Documents/depot/react/bundle \
	-p 8080:80 \
	meteorhacks/meteord:base
```

if you do not use docker:
`sudo MONGO_URL=mongodb://localhost:27017/myapps ROOT_URL=http://localhost PORT=80 node main.js`

then you can seed your data-base. Precise the needed data-base:
```
use myapps
db.users.insert({ _id : "1", createdAt: new Date(), services:{ password:{ bcrypt : "$2a$10$1MaU5IbEknr6VvCTwFptQOMQnHK/xzryMFnof58kJoRMdMmmCtCAe"     },}, username: "ede" });
```