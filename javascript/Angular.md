#### BASICS

Installer angular en ligne de commande
`npm install -g @angular/cli`

Création d'un projet
`ng new <project_name>`

Lancement de l'application
`ng serve`
or
`npm start`

Créer un composant
`ng generate component <mon_composant>`

Option au moment de la création d'un composant
`g`			: abréviation de generate''
`c`			: abréviation de component''
`--flat`			: créé le composant directement à la racine du dossier courant''
`--skip-tests`		: ne créé pas de fichier pour la création de test unitaire''
`--inline-style`		: ne créé pas de fichier css externe''
`--inline-template`	: ne créé pas de template html externe''

configuration d'angular par defaut
`./angular.json`

meta-donnée présente dans @component
```json
{
  selector: undefined,
  inputs: undefined,
  outputs: undefined,
  host: undefined,
  exportAs: undefined,
  moduleId: undefined,
  providers: undefined,
  viewProviders: undefined,
  changeDetection: ChangeDetectionStrategy.Default,
  queries: undefined,
  templateUrl: undefined,
  template: undefined,
  styleUrls: undefined,
  styles: undefined,
  animations: undefined,
  encapsulation: undefined,
  interpolation: undefined,
  entryComponents: undefined
}
```

Références
```js
<input #my_input ></input>
<button (click)=show_my_input(my_input) ></button>

public show_my_input(reference: HTMLInputElement) {
	console.log(reference)
}
```

Récupérer un ou plusieurs élement(s) depuis la class
```js
// RÉCUPÈRE UN ÉLÉMENT
@ViewChild("myinput") public el!: ElementRef<HTMLInputElement>;
// RÉCUPÈRE PLUSIEURS ÉLÉMENTS
@ViewChildren(FruitComponent) public list!: QueryList<FruitComponent>;

public show_element() {
	console.info(this.el.nativeElement.value);
}
```

Création de getter
```js
get name() {
	return this.form.get('name')
}
```

```html
<div *ngIf="name.errors && name.touched">
	...
</div>
```

---
#### ATTENTION

les **snapshots** sont l'état de la route active au moment où le constructeur est invoqué. L'utilisation du snapshot ne donne pas accès aux mises à jour au fur à mesure que l'on navigue.

Le **point d'exclamation** indique à typescript que l'on est certain que nous n'aurons pas de problème.

**BrowserModule** doit être importé uniquement dans le module racine.
Tous les autres modules doivent importer uniquement **CommonModule**. 
#### ERREUR CONNU

`Error: Cannot find module 'ajv'`
solution :
`rm -rf node_modules`
`npm cache verify`
`npm install`

---

#### AJOUTER DU CONTENU DANS UN COMPOSANT

`mon_composant.html`
```html
<div>
	<ng-content></ng-content>
</div>
```
`mon_composant_parent.html`
```html
<mon_composant>
	je peux ajouter du contenu
</mon_composant>
```

pour indiquer où est le continu selon la class
`<ng-content select=".ma-classe"></ng-content>`

**@ContentChild** permet de récupérer le contenu d'un composant enfant.
Cela fonctionne avec **ng-content**.
`@ContentChild("ref") public el!: ElementRef<HTMLParagraphElement>;`

---
récupérer l'index d'un élément avec **ngFor**
`*ngFor="let cocktail of cocktails; let index = index"`

---
#### ÉMETTRE UN ÉVÊNEMENT D'UN ENFANT VERS SON PARENT

`cocktail-list.component.ts`
```js
@Output() private changeCocktail: EventEmitter<number> = new EventEmitter<number>();

public selectCocktail(index: number): void {
  this.changeCocktail.emit(index);
}
```

Récupération de l'éméteur
`cocktail-container.component.html`
```js
<app-cocktail-list
  (changeCocktail)="selectCocktail($event)"
  [cocktails]="cocktails"
  class="flex-50 pr-10"
></app-cocktail-list>
```
`cocktail-details.component.ts`
```js
public selectCocktail(index: number): void {
  this.selectedCocktail = this.cocktails[index];
}
```

---
#### CRÉATION D'UNE DIRECTIVE D'ATTRIBUT
`ng g d color`

`color.directive.ts`
```js
import { Directive, ElementRef } from '@angular/core';

@Directive({
	selector: '[appColor]'
})
export class changeColor {
  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = 'red';
  }
}
```
`mon_composant.component.html`
```html
<div appColor></div>
```
puis ajouter la directive à la liste des déclarations dans `app.module.ts`

HostListener permet d'ajouter de la logique à une directive
`color.directive.ts`
```js
import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
	selector: '[monSurlignage]'
})
export class SurlignerDirective {

  constructor(private el: ElementRef) {}

  @HostListener('mouseenter') onMouseEnter() {
    this.surligner('red');
  }

  private surligner(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }
}
```

**HostBinding** permet de lier un attribut, un style ou une classe de l’élément hôte à une variable.
Il fonctionne de la même manière qu’une liaison de propriété définie sur un template, excepté que cela cible l’élément hôte spécifiquement.
**exemple :**
Dans **cocktail-list.component.ts** nous ajoutons la nouvelle propriété passée en **@Input()** par le composant parent : 
`cocktail-list.component.ts`
```js
[...]

export class CocktailListComponent implements OnInit {
  @Input() public cocktails!: Cocktail[];
  @Input() selectedCocktail!: Cocktail;

  @Output() private changeCocktail: EventEmitter<number> = new EventEmitter<
    number
  >();

  constructor() {}

  ngOnInit(): void {}

  public selectCocktail(index: number): void {
    this.changeCocktail.emit(index);
  }
}
```

`selected-directive.ts`
```js
[import ...]

@Directive({
  selector: "[appSelected]"
})
export class SelectedDirective implements OnChanges {
  @Input() public appSelected?: boolean;
  @HostBinding("style.backgroundColor") private backgroundColor: string;
  @HostBinding("style.color") private color: string;
  @HostBinding("style.fontWeight") private fontWeight: string;

  ngOnChanges(): void {
    if (this.appSelected) {
      this.backgroundColor = "var(--primary)";
      this.color = "white";
      this.fontWeight = "500";
    } else {
      this.backgroundColor = "white";
      this.fontWeight = "400";
      this.color = "var(--text-regular)";
    }
  }

  constructor() {
    this.appSelected = false;
    this.backgroundColor = 'white';
    this.fontWeight = '400';
    this.color = 'var(--text-regular)';
  }
}
```

nous lions les propriétés de style **backgroundColor**, **fontWeight** et **color** à des variables pour pouvoir les modifier, et ce grâce à **@HostBinding**. 
```js
@HostBinding("style.backgroundColor") private backgroundColor: string;
@HostBinding("style.color") private color: string;
@HostBinding("style.fontWeight") private fontWeight: string;
```

---
#### SERVICE ET INJECTION DE DÉPENDANCES
`ng g s aliments`

l'inlejection de dépendence est l'injection d'instance dans dans des composants et non de classes. L'avantage, c'est que la modification de cette instance modifie tout les composants dans la-quelle elle a été injecté.

la dépendance doit être déclarée dans les **providers** et fournie en paramètre du constructeur de la classe où elle est appellé.

On l'utilise notemment pour créer des **services**.

`aliments.service.ts`
```js
import { Injectable } from "@angular/core";

@Injectable()
export class AlimentService {
	public aliments: string[] = [];
	
	constructor() {}
}
```
préciser que le service est injectable avec le décorateur **@injectabre()**

`app.module.ts`
```js
import { AlimentService } from "./aliment.service";

@NgModule({
	imports: [BrowserModule, FormsModule],
	declarations: [AppComponent, AddAlimentComponent, ListAlimentComponent],
	bootstrap: [AppComponent],
	providers: [AlimentService]
})
```

`list-aliments.component.ts`
```js
import { Component } from '@angular/core';
import { AlimentService } from '../aliment.service';

@Component({
	selector: 'app-list-aliment',
	templateUrl: './list-aliment.component.html',
	styleUrls: ['./list-aliment.component.scss']
})

export class ListAlimentComponent {
	public aliments: string[] = this.alimentService.aliments;
	
	constructor(private alimentService: AlimentService) {}
}
```

**@Injectable** peut prendre la propriété **provideIn** qui définie l'endroit à partir du quel le service est injecté. Il n'y a donc plus besoin de le déclarer dans les providers.
```js
@Injectable({
  providedIn: 'root',
})
```

---
#### OBSERVER ET OBSERVABLE

création d'un observable
```js
const observable = new Observable(observer => {
  observer.next('1');
  observer.next('2');
});
monObservable.subscribe(value => console.log(value));
```

désinscription
```js
const subscription = observable.subscribe(x => console.log(x));
subscription.unsubscribe(); // arrêt de l’exécution de l’observable
```

on peut préciser ce que l'observer retourne
```js
const observer = {
  next: x => console.log('Observer a reçu une valeur : ' + x),
  error: err => console.error('Observer a reçu une erreur : ' + err),
  complete: () => console.log('Observer a reçu une notification de complétion')
};
```
si on ne met rien, l'observer retourne **next** par defaut

---
#### CREATION D'UN SERVICE

`cocktail.service.ts`
```js
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Cocktail } from "../interfaces/cocktail.interface";

@Injectable({ providedIn: "root" })
export class CocktailService {
  public cocktails$: BehaviorSubject<Cocktail[]> = new BehaviorSubject([
    // les cocktails ...
  ]);

  public selectedCocktail$: BehaviorSubject<Cocktail> = new BehaviorSubject(
    this.cocktails$.value[0]
  );

  public selectCocktail(index: number): void {
    this.selectedCocktail$.next(this.cocktails$.value[index]);
  }

  constructor() {}
}
```
**next()** va modifier la valeur de l'observable ( c'est comme une affectation mais pour les souscription )
**BehaviorSubject** garde en mémoire la dernière valeur du **subject**

**this.cocktails$** le dollard à la fin de la variable indique qu'il s'agit d'un observable. C'est une convention.

1. Le service est instancié dans le constructor
2. nous créons une instance de subscription pour y ajouter toutes les subscriptions
3. puis on souscrit aux observables du service
`cocktail-container.component.ts`
```js
[...]

export class CocktailContainerComponent implements OnInit, OnDestroy {
  public selectedCocktail!: Cocktail;
  public cocktails!: Cocktail[];
  public subscription: Subscription = new Subscription();

  constructor(private cocktailService: CocktailService) {}

  ngOnInit() {
    this.subscription.add(
      this.cocktailService.cocktails$.subscribe((cocktails: Cocktail[]) => {
        this.cocktails = cocktails;
      })
    );
    this.subscription.add(
      this.cocktailService.selectedCocktail$.subscribe(
        (selectedCocktail: Cocktail) => {
          this.selectedCocktail = selectedCocktail;
        }
      )
    );
  }

  public selectCocktail(index: number) {
    this.cocktailService.selectCocktail(index);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
```
**ngOnDestroy()** => on se désinscrit quand le composant est détruit.

---
#### ROOTING

`index.html`
```html
<head>
<base href="/">
</head>
```

`app.module.ts`
```js
imports [...]
import { RouterModule, Routes } from '@angular/router';

const APP_ROUTES: Routes = [
  { path: "", component: HomepageComponent },
  { path: "users", component: UsersComponent }
];

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(APP_ROUTES)],
  declarations: [...],
  bootstrap: [...],
  providers: []
})

export class AppModule {}
```
Attention ! L'ordre est important : le Router utilise la stratégie du premier match l'emporte.
Cela veut dire qu'il faut placer les routes les plus spécifiques avant les routes plus générales.

`app.component.html`
```html
<div class="conatiner d-flex flex-column" style="padding-top: 150px">
	<nav>
		<ul>
			<li routerLink="/" routerLinkActive="active" [routerLinkActiveOptions]="{exact:true}">Homepage</li>
			<li routerLink="/users" routerLinkActive="active">Users</li>
		</ul>
	</nav>
	<div>
		<router-outlet></router-outlet>
	</div>
</div>
```
`router-outlet` permet au Router de savoir où placer les composants.
`routerLink` permet de faire sur quelle route sera rendu le composant.
`routerLinkActive` définie la classe qui nous permetras d'identifier quelle route est active lorsque l'on clique sur les onglets.
`routerLinkActiveOptions="{exact:true}"` ajoutes une option qui dit que la route est active s'il est correspond strictement à `/`. Dans le cas contraire la route active correspond à toutes les routes qui commencent par `/`.

pour remoter d'un cran dans l'url
`routerLink="../"`

###### REDIRECTION

`mon_composant.ts`
```js
// import [...]

// @component
export class HomepageComponent implements OnInit {
  constructor(private router: Router) {}

  public redirection() {
    // this.router.navigate(["users"]);
    this.router.navigateByUrl("/users");
  }
}
```

`mon_composant.html`
```html
<h1 class="m-10">Homepage</h1>
<p class="m-10" (click)="redirection()">
  Lorem ipsum
</p>
```

Pour naviguer de façon relative :
`router.navigate(['../index', ], {relativeTo: route});`

###### PARAMÈTRES

déclaration d'un paramêtre
`app.module.ts`
```js
export const routes: Routes = [
  { path: 'details-produit/:id', component: DetailsProduitComponent }
]
```

`app.list-produit.html`
```html
<a [routerLink]="['/details-produit', 5]"> Mon produit 5 </a>
```

exemple de liste
```html
<a *ngFor="let produit of produits"
    [routerLink]="['/details-produit', produit.id]">
    {{ produit.nom }}
</a>
```

###### ACCÉDER À  DES INFORMATIONS SUR UNE ROUTE

`ActivatedRoute` est un service qui contient des informations spécifiques à la route : comme les paramètres, les données statiques et résolues, le fragment global.

- `url`: un Observabledu pathde la route qui contient un tableau de chaîne de caractères pour chaque partie de la route
- `data`: un Observablequi contient l'objet dataqui peut optionnellement être passé à la route 
- `paramMap`: un Observablequi contient un ParamMapdepuis lequel on peut accéder aux paramètres à la route
- `queryParamMap`: un Observablequi contient un ParamMapdepuis lequel on peut accéder à tous les paramètres disponibles pour toutes les routes
- `fragment`: un Observablequi contient le fragment disponible pour toutes les routes
- `oulet`: le nom du RouterOutletutilisé pour rendre les vues associées à la route. Par défaut, le nom est primary
- `routeConfig`: la configuration de la route qui contient le chemin d'origine
- `parent`: contient les informations de la route parente lorsque sont utilisées des routes enfants
- `firstChild`: contient les informations de la première route enfant dans la liste des routes enfants
- `children`: contient toutes les informations de toutes les routes enfants de la route

`user.component.ts`
```js
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";

// @Component({...})
export class UserComponent implements OnInit {
  public id?: string;
  public name?: string;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      this.id = paramMap.get("id")!;
      this.name = paramMap.get("name")!;
    });
  }
}
```

`user.component.html`
```html
<h1 class="m-10">USERS</h1>
<ul class="m-10 list-primary">
	<li [routerLink]="[user.id, user.name]" *ngFor="let user of users">{{user.name}}</li>
</ul>
```

`app.module.ts`
```js
// import [...]
import { RouterModule, Routes } from "@angular/router";

const APP_ROUTES: Routes = [
  // { ... },
  { path: "users/:id/:name", component: UserComponent }
];

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(APP_ROUTES)],
  declarations: [...],
  bootstrap: [...],
  providers: []
})
export class AppModule {}
```

###### PARAMÈTRES OPTIONNELS ET FRAGMENTS

1. `queryParams` pour passer des paramètres optionnels dans les templates
```html
<a [routerLink]="['liste-produits']" [queryParams]="{ page: 2 }">Aller page 2</a>
```

2. `queryParams` pour passer des paramètres optionnels dans les classes
```js
allerPage(page: number) {
  this.router.navigate(['/liste-produits'], { queryParams: { page: page } });
}
```

`queryParamMap` pour accéder aux paramètres optionnels
```js
this.route.queryParamMap.subscribe((params: ParamMap) => this.page = params.get('page');
```

`fragment` permet de se référer à certains éléments de la page identifiés par un attribut id. Naviguer en utilisant un fragment permet de se rendre directement au niveau de l'élément sur la page.
```js
allerPage(page: number) {
  this.router.navigate(['/liste-produits'], { queryParams: { page: page }, fragment: 'footer' });
}
```

###### EXEMPLE

`users.component.html`
```html
<h1 class="m-10">USERS</h1>
<ul class="m-10 list-primary">
	<li (click)="redirection(user)" *ngFor="let user of users">{{user.name}}</li>
</ul>
```

`user.component.ts`
```js
import { ... }
import { ActivatedRoute, ParamMap } from "@angular/router";

@Component({ ... })
export class UserComponent implements OnInit {
  public id?: string;
  public name?: string;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      this.id = paramMap.get("id")!;
    });
    this.activatedRoute.queryParamMap.subscribe((paramMap: ParamMap) => {
      this.name = paramMap.get("name")!;
    });
    this.activatedRoute.fragment.subscribe((fragment: string | null) => {
      console.log(fragment);
    });
  }
}
```

`users.component.ts`
```js
import { ... }
import { ActivatedRoute, Router } from "@angular/router";

interface User {
  id: number;
  name: string;
}

@Component({ ... })
export class UsersComponent implements OnInit {
  public users: User[] = [
    { name: "Pierre", id: 1 },
    { name: "Paul", id: 2 },
    { name: "Jacques", id: 3 }
  ];
  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {}

  redirection(user: User) {
    this.router.navigate([user.id], {
      relativeTo: this.activatedRoute,
      queryParams: {
        name: user.name
      },
      fragment: "foo"
    });
  }
}
```

`app.module.ts`
```js
import { ... }
import { RouterModule, Routes } from "@angular/router";

const APP_ROUTES: Routes = [
  { path: "", component: HomepageComponent },
  { path: "users", component: UsersComponent },
  { path: "users/:id", component: UserComponent }
];

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(APP_ROUTES)],
  declarations: [...],
  bootstrap: [...],
  providers: []
})
export class AppModule {}
```

#### ROUTES IMBRIQUÉES ET PRÉSERVATION DES PARAMÈTRES OPTIONNELS

Il est possible de préserver les paramètres optionnels et le fragment. Pour ce faire, il est nécessaire d'utiliser queryParamsHandling et/ou preserveFragment.
On peut importer NavigationExtras depuis @angular/router si l'on souhaite typer cette configuration dans une propriété.
```js
let configNavigation: NavigationExtras = {
  queryParamsHandling: 'merge',
  preserveFragment: true
};

// Navigation à la page de login en conservant les paramètres/fragment :
this.router.navigate(['/login'], configNavigation);
```

routes imbriquées grâce à l'attribut **children**
```js
export const routes: Routes = [
  { path: 'liste-produits', component: ListeProduitsComponent },
  { path: 'details-produit/:id', component: ProduitComponent,
    children: [
      { path: 'details', component: DetailsProduitComponent },
      { path: '', component: GallerieComponent }
    ]
  }
];
```

###### EXEMPLE

`app.module.ts`
```js
import { ... }

const APP_ROUTES: Routes = [
  { path: "", component: HomepageComponent },
  {
    path: "users",
    component: UsersComponent,
    children: [{ path: ":id", component: UserComponent }]
  }
];

@NgModule({ ... })
export class AppModule {}
```

un **router-outlet** dans un sous-composant permet d'afficher les routes enfants
`users.component.html`
```html
<h1 class="m-10">USERS</h1>
<ul class="m-10 list-primary">
	<li (click)="redirection(user)" *ngFor="let user of users">{{user.name}}</li>
</ul>
<router-outlet></router-outlet>
```

Nous allons ajouter une propriété **details**, pour montrer la fusion des paramètres optionnels entre route parente et route enfant.
`user.component.html`
```html
<h1 class="m-10">User</h1>
<ul class="list-primary">
	<li>{{id}}</li>
	<li>{{name}}</li>
	<li>details: {{details}}</li>
</ul>
```

On récupère la propriété dans la classe
`user.component.ts`
```js
import { ... }

@Component({ ... })
export class UserComponent implements OnInit {
  public id?: string;
  public name?: string;
  public details?: string;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      this.id = paramMap.get("id")!;
    });
    this.activatedRoute.queryParamMap.subscribe((paramMap: ParamMap) => {
      this.name = paramMap.get("name")!;
      this.details = paramMap.get("details")!;
    });
    this.activatedRoute.fragment.subscribe((fragment: string | null) => {
      console.log(fragment);
    });
  }
}
```

Nous définissons un **queryParam** au plus haut niveau, dans `app.component.html`, simplement pour tester la fusion
```html
<div class="container d-flex flex-column" style="padding-top: 150px">
	<nav>
		<ul>
			<li routerLink="/" routerLinkActive="active" [routerLinkActiveOptions]="{exact:true}">Homepage</li>
			<li routerLink="/users" [queryParams]="{details: true}" routerLinkActive="active">Users</li>
		</ul>
	</nav>
	<div>
		<router-outlet></router-outlet>
	</div>
</div>
```

Il ne reste plus qu'à configurer la fusion des paramètres optionnels dans `users.component.ts`
```js
import { ... }

interface User {
  id: number;
  name: string;
}

@Component({ ... })
export class UsersComponent implements OnInit {
  public users: User[] = [
    { name: "Pierre", id: 1 },
    { name: "Paul", id: 2 },
    { name: "Jacques", id: 3 }
  ];
  constructor(private router: Router, private activatedRoute: ActivatedRoute) {}

  ngOnInit() {}

  redirection(user: User) {
    this.router.navigate([user.id], {
      relativeTo: this.activatedRoute,
      queryParams: {
        name: user.name
      },
      queryParamsHandling: "merge",
      fragment: "foo"
    });
  }
}
```

#### REDIRECTION ET WIlDCARD

l'ordre des chemins est important. Toujours mettre la wildcard à la fin
- afficher un composant d'erreur : `{ path: '**', component: PageNonTrouveeComponent }`
- afficher un composant par défaut : `{ path: '**', component: HomeComponent }`
- rediriger : `{ path: '**', redirectTo: '/home' }`



#### GARDES

###### canActivate
**canActivate** permet de contrôler l'accès d'un utilisateur à une route.

`app.component.ts`
```js
const appRoutes: Routes = [
  {
      path: 'dashboard',
      component: DashboardComponent,
      canActivate: ['AuthGuard']
  },
];
```

`auth.guard.ts`
```js
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService) {}

  canActivate(): boolean {
    return this.authService.isLoggedIn();
  }
}
```

autre exemple avec une redirection
`auth.guard.ts`
```js
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router){}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(["/login"]);
      return false;
    } else {
        return true;
    }
  }
}
```

Protéger l'accès des routes enfants avec `CanActivateChild`
`auth.guard.ts`
```js
import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  RouterStateSnapshot
} from "@angular/router";

@Injectable({ providedIn: "root" })
export class AuthGuard implements CanActivate, CanActivateChild {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    console.log(route, state);
    return true;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return true;
  }
}
```

`app-routing.module.ts`
```js
...
export const APP_ROUTES: Routes = [
  { path: "", component: HomepageComponent },
  {
    path: "users",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: UsersComponent,
    children: [{ path: ":id", component: UserComponent }]
  },
  { path: "**", redirectTo: "" }
];
...
```
les guardes sont des services et doivent être ajouté aux providers

###### canDeactivate
**CanDeactivate** permet d'éviter que les changements non sauvegardés soient perdus.

```js
import { CanDeactivate } from '@angular/router';
import { Observable }    from 'rxjs;

export interface CanComponentDeactivate {
 canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
  canDeactivate(component: CanComponentDeactivate) {
    return component.canDeactivate ? component.canDeactivate() : true;
  }
}
```
**CanComponentDeactivate** est une interface TypeScript classique que l'on utilise pour typer un composant.
Si une méthode **canDeactivate()** existe sur le composant, alors retourne la valeur de retour de cette méthode et dans le cas contraire retourne **true**

###### exemple

`ng g c user-edit`

`user-edit.component.html`
```html
<h1 class="m-10">User Edit</h1>
<div class="m-10 d-flex flex-row">
	<input type="text" placeholder="name">
	<button (click)="save = true" class="btn btn-primary">Save</button>
</div>
```

`user-edit.component.ts`
```js
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-user-edit",
  templateUrl: "./user-edit.component.html",
  styleUrls: ["./user-edit.component.scss"]
})
export class UserEditComponent implements OnInit {
  public save: boolean = false;

  canDeactivate(): boolean {
    if (this.save) {
      return true;
    } else {
      return confirm("Êtes-vous certain de vouloir quitter ?");
    }
  }

  constructor() {}

  ngOnInit() {}
}
```

`app-routing.module.ts`
```js
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { Routes } from "@angular/router";
import { AuthGuard } from "./auth.guard";
import { HomepageComponent } from "./homepage/homepage.component";
import { UserEditComponent } from "./user-edit/user-edit.component";
import { UserComponent } from "./user/user.component";
import { UsersComponent } from "./users/users.component";

export const APP_ROUTES: Routes = [
  { path: "", component: HomepageComponent },
  {
    path: "users",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: UsersComponent,
    children: [
      {
        path: ":id/edit",
        canDeactivate: [AuthGuard],
        component: UserEditComponent
      },
      { path: ":id", component: UserComponent }
    ]
  },
  { path: "**", redirectTo: "" }
];
@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
```

`auth.guard.ts`
```js
import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  CanDeactivate,
  RouterStateSnapshot
} from "@angular/router";
import { UserEditComponent } from "./user-edit/user-edit.component";

@Injectable({ providedIn: "root" })
export class AuthGuard
  implements CanActivate, CanActivateChild, CanDeactivate<UserEditComponent> {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    console.log(route, state);
    return true;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return true;
  }

  canDeactivate(
    component: UserEditComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    return component.canDeactivate();
  }
}
```

###### resolve
**resolve** permet de précharger des données pour les utiliser dans un composant
un **resolver** est appelé avant que la navigation soit enclenchée. Le composant affiché précédemment reste affiché jusqu'à ce que les données soient chargées. Ce n'est qu'à ce moment là que la navigation est enclenchée : il n'y a donc pas de blanc le temps du chargement !

`app-routing.module.ts`
```js
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { Routes } from "@angular/router";
import { AuthGuard } from "./auth.guard";
import { HomepageComponent } from "./homepage/homepage.component";
import { UserEditComponent } from "./user-edit/user-edit.component";
import { UserComponent } from "./user/user.component";
import { UsersComponent } from "./users/users.component";

export const APP_ROUTES: Routes = [
  { path: "", component: HomepageComponent },
  {
    path: "users",
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    component: UsersComponent,
    children: [
      {
        path: ":id/edit",
        canDeactivate: [AuthGuard],
        component: UserEditComponent
      },
      {
        path: ":id",
        data: { title: "User list" },
        resolve: { user: AuthGuard },
        component: UserComponent
      }
    ]
  },
  { path: "**", redirectTo: "" }
];
@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
```

`user.component.ts`
```js
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { User } from "../users/users.component";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"]
})
export class UserComponent implements OnInit {
  public id?: string;
  public name?: string;
  public details?: string;

  constructor(private activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      this.id = paramMap.get("id")!;
    });

    this.activatedRoute.queryParamMap.subscribe((paramMap: ParamMap) => {
      this.name = paramMap.get("name")!;
      this.details = paramMap.get("details")!;
    });

    this.activatedRoute.fragment.subscribe((fragment: string | null) => {
      console.log(fragment);
    });

    this.activatedRoute.data.subscribe(
      (data) => {
        console.log(data);
      }
    );
  }
}
```
Nous utilisons la propriété data sur la route active pour accéder aux données statiques et asynchrones

`auth.guard.ts`
```js
import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanActivateChild,
  CanDeactivate,
  Resolve,
  RouterStateSnapshot
} from "@angular/router";
import { UserEditComponent } from "./user-edit/user-edit.component";
import { User } from "./users/users.component";

@Injectable({ providedIn: "root" })
export class AuthGuard
  implements
    CanActivate,
    CanActivateChild,
    CanDeactivate<UserEditComponent>,
    Resolve<User> {
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    console.log(route, state);
    return true;
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return true;
  }

  canDeactivate(
    component: UserEditComponent,
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    return component.canDeactivate();
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): User {
    const userId = route.paramMap.get("id");
    return {
      id: 5,
      name: "Julie"
    };
  }
}
```
On ajoute enfin la méthode **resolve()** qui simule la récupération d'un utilisateur de manière asynchrone

#### FORMULAIRES

Pour pouvoir utiliser les directives et les classes natives d'Angular pour les formulaires réactifs, il faut importer : **ReactiveFormsModule** dans `app.modules.ts`

**ngSubmit** permet de récupérer l'évènement lié à un formulaire pour l'envoyer vers un serveur.
```html
<form [formGroup]="mon_form" (ngSubmit)="submit()">
	...
</form>
```
Un bouton par default soumet le formulaire.
`type="button"` permet d'éviter la soumission du formulaire quand on clique sur un bouton.

###### Les classes natives pour les formulaires Angular
**AbstractControl** : c'est la classe de base de laquelle les trois autres classes principales pour les contrôles sur le formulaire découlent. Elle permet d'apporter aux autres classes des fonctionnalités essentielles : utilisation de validateurs, calcul des statuts du formulaire, modification des statuts, réinitialisation etc.
**FormControl** : c'est la classe qui permet de suivre la valeur et la validité d'un contrôle du formulaire.
**FormGroup** : c'est la classe qui permet de suivre la valeur et la validité d'un groupe d'instances **FormControl**.
**FormArray** : c'est la classe qui permet de suivre la valeur et la validité d'un tableau d'instances **AbstractControl**. 

Un **FormGroup** prend comme premier argument un ensemble de **FormControl**:
```js
const form = new FormGroup({
 premier: new FormControl('valeurInitiale', validateur, validateur asynchrone),
 deuxieme: new FormControl('valeurInitiale', validateur, validateur asynchrone)
});
```
En deuxième et troisième argument il prend respectivement un ou plusieurs validateurs, et un ou plusieurs validateurs asynchrones qui seront appliqués sur le groupe entier.

###### AbstractControl
**validator** retourne une fonction de validation
**asyncValidator** retourne une fonction de validation asynchrone
**value** retourne la valeur
**parent** retourne le FormGroup|FormArray parent ou undefined
**status** retourne une chaîne de caractères correspondant au statut du contrôle VALID/INVALID/PENDING/DISABLED
**valid** retourne un booléen suivant la valeur de status === VALID
**invalid** retourne un booléen suivant la valeur de status === INVALID
**pending** : retourne un booléen suivant la valeur de status === PENDING
**disabled** : retourne un booléen suivant la valeur de status === DISABLED
**enabled** : retourne un booléen suivant la valeur de status !== DISABLED
**pristine** : retourne true si l'utilisateur n'a pas modifié la valeur
**dirty** : retourne true si l'utilisateur a modifié la valeur
**touched** : retourne true si l'utilisateur a touché le champ
**untouched** : retourne true si l'utilisateur n'a pas touché le champ
**errors** : retourne les erreurs de validation
**valueChanges** : retourne un Observable qui émet la valeur du control à chaque fois qu'elle change
**statusChanges** : retourne un Observable qui émet la valeur du status du control à chaque fois qu'elle change
**updateOn** : retourne la valeur de la stratégie de mise à jour du control qui peut être 'change', 'blur' ou 'submit'
**root** : récupère l'ancêtre de l'AbstractControl .
**setValidators(newValidator: ValidatorFn | ValidatorFn[])** : permet de définir les validateurs synchrones.
**setAsyncValidators(newValidator: AsyncValidatorFn | AsyncValidatorFn[])** : permet de définir les validateurs asynchrones.
**clearValidators()** : permet de supprimer les validateurs synchrones.
**clearAsyncValidators()** : permet de supprimer les validateurs asynchrones.
**markAsTouched()** : permet de marquer le control comme étant touché.
**markAllAsTouched()** : permet de marquer le control et tous ses descendants comme étant touchés.
**markAsUntouched()** : permet de marquer le control et tous ses descendants comme n'étant pas touchés.
**markAsDirty()** : permet de marquer le control comme étant dirty.
**markAsPristine()** : permet de marquer le control comme étant pristine.
**markAsPending()** : permet de marquer le control comme étant pending.
**disable()** : permet de désactiver le control.
**enable()** : permet de réactiver le control.
**setParent(parent: FormGroup | FormArray)** : permet de définir le parent ducontrol.
**setValue(value)** : permet de définir la valeur ducontrol.
**patchValue(value)** : permet de définir la valeur ducontrol.
**reset(value?)** : permet de réinitialiser le control.
**updateValueAndValidity()** : permet de modifier la valeur et de recalculer le statut du control.
**setErrors(errors: ValidationErrors)** : permet définir les erreurs du control.
**get(path: string | (string | number)[])** : permet de récupérer un control enfant en passant son nom ou son chemin.  **getError(errorCode: string, path?: string)** : de récupérer une erreur spécifiée.
**hasError(errorCode: string, path?: string)** : permet de savoir si un control a l'erreur spécifiée en argument.

###### FormGroup
La classe **FormGroup** est une classe fille de la classe **AbstractControl** :  **class FormGroup extends AbstractControl**

**controls** : retourne un objet avec tous les **formControl** du **formGroup**
**addControl(name, control)** : permet d'ajouter un **formControl** passé en paramètre au **formGroup** en mettant à jour la valeur et la validité du **formControl**
**registerControl(name, control)** : permet d'ajouter un **formControl** passé en paramètre au **formGroup** sans mettre à jour la valeur et la validité du **formControl**
**removeControl(name)** : permet de supprimer le **formControl** dont le nom est passé en paramètre au **formGroup**
**setControl(name, control)** : permet de remplacer un **formControl** du **formGroup**
**contains(name)** : permet de retourner un booléen suivant si un **formControl** existe sur le **formGroup** et est actif
**setValue(value)** : permet de définir la valeur du **formGroup** en passant un objet contenant des **formControl** correspondant à la structure du **formGroup**. L'objet passé doit être de la forme **{premierFormControl: 'valeur', deuxiemeFormControl: 'valeur'}**
**patchValue(value)** : permet de définir la valeur de certains **formControl** du **formGroup** en passant un objet contenant des **formControl**. L'objet passé doit être de la forme vu ci-dessus.
**reset(value?)** : permet de réinitialiser le **formGroup** en lui passant éventuellement un objet de valeurs initiales pour les **formControl**. L'objet passé doit être de la forme vu ci-dessus.
**getRawValue()** : permet de récupérer la valeur de tous les controls du **formGroup** y compris ceux qui sont désactivés (contrairement à value).

###### FormControl
La classe **FormControl** est une classe fille de la classe **AbstractControl**.

**setValue(value)** et **patchValue(value)** : permet de définir la valeur du **formControl**
**reset(value?)** : permet de réinitialiser le formControl, en lui passant éventuellement une valeur initiale
**registerOnChange(fn: Function)** : permet d'enregistrer un écouteur qui va exécuter une fonction à chaque changement de valeur
**registerOnDisabledChange(fn: (isDisabled: boolean) => void)** : permet d'enregistrer un écouteur qui va exécuter une fonction à chaque événement d'activation ou de désactivation.

**setValue** prend en paramètre tous les control présent dans le **FormGroup**
**patchValue** peut ne prendre qu'un seul control parmis tous ceux du **FormGroup**, par contre aucune erreur de ne sera soulevée si la structure du control n'est pas respectée.
**reset** permet de retournée à l'état **pristine** ( initiale, jamais touché )

###### Validateurs
Pour utiliser les validateurs, il faut importer **Validators** depuis **@angular/form**.

Pour utiliser plusieurs validateurs
```js
public monForm: FormGroup = new FormGroup ({
  name: new FormControl('', [Validators.required, Validators.minLength(4)])
  ...
});
```

vérifier que le mail et la confirmation de du mail correspondent
```js
public monForm: FormGroup = new FormGroup(
  {
    name: new FormControl(""),
    email: new FormControl("", Validators.email),
    confirmEmail: new FormControl("")
  },
  { validators: this.emailsMatch() }
);

emailsMatch(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    return control.get('email')!.value != control.get('confirmEmail')!.value
      ? { noMatch: true }
      : null;
  };
}
```

###### validateurs asynchrones

exemple : un **Validator** qui va vérifier en plus si le **SMTP** de l'adresse email existe
```js
public form: FormGroup = new FormGroup(
  {
    name: new FormControl(''),
    email: new FormControl(
      '',
      [Validators.required, Validators.email],
      this.asyncEmailValidator()
    ),
    confirmEmail: new FormControl('')
  },
  { validators: this.emailsMatch() }
);

asyncEmailValidator(): AsyncValidatorFn {
  return (control: AbstractControl): Observable<ValidationErrors | null> => {
    return this.http.get('https://apilayer.net/api/check?access_key=115ce3ae2ccdef30e018edbde78d2c4a&email=' + control.value + '&smtp=1&format=1')
    .pipe(
      map((response: any) => {
        return !response.smtp_check ? { asyncEmailValidator: control.value } : null;
      }));
  }
}
```

###### messages d'erreurs

```html
<div *ngIf="erreursForm['name']" class="error">
  {{ erreursForm['name'] }}
</div>
```

```js
public erreursForm: { [field: string]: string } = {
  name: "",
  email: "",
  confirmEmail: "",
  form: ""
};

public messagesErreur: { [field: string]: { [field: string]: string } } = {
  name: {
    required: "Ce champ est requis.",
    minlength: "Vos nom et prénom doivent faire au moins 4 caractères."
  },
  email: {
    required: "Entrez un email.",
    email: "Rentrez une adresse email valide.",
    asyncEmailValidator: "L'email n'existe pas."
  },
  confirmEmail: {
    email: "Rentrez une adresse email valide."
  },
  form: {
    noMatch: "Les emails ne correspondent pas."
  }
};
```
on souscrit à un observable
```js
this.subscription.add(
  this.form.statusChanges.subscribe(() => {
    this.changementStatusForm();
  })
);
```
A chaque fois que le statut du **formGroup** changera, on recalcule les erreurs à afficher grâce à la fonction **changementStatusForm**.
```js
changementStatusForm() {
  if (!this.form) {
    return;
  }
  const form = this.form;
  for (const field in this.erreursForm) {
    this.erreursForm[field] = '';
    let control: AbstractControl;
    if (
      field === 'form' &&
      form.get('email')!.touched &&
      form.get('confirmEmail')!.dirty
    ) {
      control = form;
    } else {
      control = form.get(field)!;
    }
    if (control && control.touched && control.invalid) {
      const messages = this.messagesErreur[field];
      for (const key in control.errors) {
        this.erreursForm[field] += messages[key] + ' ';
      }
    }
  }
}
```
###### Classes pour formulaires
 On peut ajouter des classes sur les éléments HTML liés aux **formGroup** et **formControl** : 
- **ng-valid** : correspond à valid
- **ng-invalid** : correspond à invalid
- **ng-pristine** : correspond à pristine
- **ng-pending** : correspond à pending
- **ng-dirty** : correspond à dirty
- **ng-touched** : correspond à touched
- **ng-untouched** : correspond à untouched

###### FormGroup imbriqué
```js
public form: FormGroup = new FormGroup({
  login: new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('')
  }),
  nom: new FormControl('', Validators.required)
});
```

```html
<form [formGroup]="form" (ngSubmit)="submit()">
  <div class="d-flex flex-column form-group">
    <label>Nom</label>
    <input
      formControlName="nom"
      type="text"
      name="name"
      placeholder="prénom nom"
    />
  </div>
  <div class="error" *ngIf="nom.errors.required && nom.touched">
    <p>Vous devez renseigner un nom</p>
  </div>
  
  <div formGroupName="login">
    <div class="d-flex flex-column form-group">
      <label>Email</label>
      <input
        formControlName="email"
        type="email"
        name="email"
        placeholder="adresse email"
      />
    </div>
    <div class="d-flex flex-column form-group">
      <label>Mot de passe</label>
      <input
        formControlName="password"
        type="password"
        name="password"
        placeholder="mot de passe"
      />
    </div>
  </div>
  
	...
	
</form>
```
###### FormArray
Un **FormArray** est particulièrement utile pour réaliser des formulaires dynamiques : c'est-à-dire avec un nombre dynamique d'**AbstractControl**. 
Comme pour les **FormGroup** vous pouvez passer en deuxième argument des validateurs synchrones pour le **FormArray**, et en troisième argument, des validateurs asynchrones.
```js
public form: FormGroup = new FormGroup({
  nom: new FormControl('', Validators.required),
  email: new FormControl('', [Validators.required, Validators.email]),
  hobbies: new FormArray([]),
  password: new FormControl('')
});
```

```html
<form [formGroup]="form" (ngSubmit)="submit()">
  ...
  <div formArrayName="hobbies" class="d-flex flex-column form-group">
    <label>Hobbies</label>
    <button type="button" class="btn btn-primary" (click)="addHobby()">
      Ajouter hobby
    </button>
      <div *ngFor="let hobby of hobbies.controls; let index = index">
      <input
        style="margin-top: 5px;"
        type="text"
        [formControlName]="index"
      />
      <span class="delete" (click)="deleteHobby(index)">&#10060;</span>
    </div>
  </div>
  ...
  <div class="btn">
    <button type="submit" class="btn btn-primary">Inscription</button>
  </div>
</form>
```
###### FormBuilder
**FormBuilder** est une classe permettant d'utiliser du sucre syntaxique pour raccourcir **new FormGroup()**, **new FormControl()**, et new **FormArray()**.
```js
import { FormBuilder } from '@angular/forms';
...
constructor(private fb: FormBuilder)
```

```js
public form: FormGroup = this.fb.group({
  nomEntier: fb.group({
    prenom: ['Jean', Validators.minLength(2)],
    nom: 'Dupont',
  }),
  email: '',
});
```
###### FormGroup et EventEmitter
**valueChanges** retourne un Observable auquel l'on peut souscrire.
exemple sur un **FormGroup**
`this.form.valueChanges.subscribe(valeur => console.log(valeur));`
Il ne faut pas oublier d'**unsubscribe()** à ces Subscriptions !
###### Cases à cocher
```js
public form: FormGroup = this.fb.group({
  nom: ['', Validators.required],
  email: ['', [Validators.required, Validators.email]],
  hobbies: this.fb.array([]),
  password: [''],
  gender: ['female'],
  majeur: [true],
});
```

```html
<div class="d-flex flex-column form-group">
  <label>Etes-vous majeur ?
    <input formControlName="majeur" type="checkbox" />
  </label>
</div>
```
###### boutons radio
```js
public form: FormGroup = this.fb.group({
  nom: ['', Validators.required],
  email: ['', [Validators.required, Validators.email]],
  hobbies: this.fb.array([]),
  password: [''],
  gender: ['female'],
});
```

```html
<div class="d-flex flex-column form-group">
  <label>Male
    <input type="radio" value="male" formControlName="gender">
  </label>
  <label>Female
    <input type="radio" value="female" formControlName="gender">
  </label>
</div>
```
###### listes déroulantes
```js
public cities = [
  { value: "paris", label: "Paris" },
  { value: "lyon", label: "Lyon" },
  { value: "nice", label: "Nice" },
  { value: "toulouse", label: "Toulouse" }
];

…

public form: FormGroup = this.fb.group({
  nom: ['', Validators.required],
  email: ['', [Validators.required, Validators.email]],
  hobbies: this.fb.array([]),
  password: [''],
  gender: ['female'],
  majeur: [true],
  city: ['paris']
});
```

```html
<div class="d-flex flex-column form-group">
  <label>Ville
    <select formControlName="city">
      <option *ngFor="let city of cities" [value]="city.value">{{city.label}}</option>
    </select>
  </label>
</div>
```




#### PIPES
Les pipes modifient l'apparence d'une valeur dans le template
`{{ variable | date:format:timezone:locale }}`
Ils peuvent prendre des paramètres séparés par **:**
###### CRÉATION DE PIPE PERSONALISÉ
`mon_pipe.pipe.ts`
```js
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'nomPourUtiliser'})
export class MonPipe implements PipeTransform {
  transform(valeur, param2, param3...): number {
    // transformation de la valeur en premier paramètre en utilisant les autres paramètres optionnels
   return maValeurTransformée;
  }
}
```
`app.module.ts`
```js
@NgModule({
imports: [...],
declarations: [
	AppComponent,
	ReversePipe
],
bootstrap: [...]
})
export class AppModule {}
```
###### PIPES PURS et IMPURS
le pipe est pur par defaut mais on peut lui indiquer qu'il est impur de cette manière
```js
@Pipe({
  name: 'nom',
  pure: false
})
```
Les pipes purs sont à privilégier dans un soucis de performance, mais parfois on a besoin de savoir si les valeurs ou les propriétés d’un objet ont changé, il faut alors utiliser un pipe impur.
###### PIPE ASYNC
Le pipe **Async** est un pipe Angular natif impur.
Il prend en input une **Promise** ou un **Observable** et y souscrit automatiquement.
Il permet de ne pas avoir à souscrire à la **Promise** ou à l’**Observable** sur la classe du composant, ce qui permet d’éliminer plusieur lignes de codes.
Deuxièmement, le pipe **Async** unsubscribe automatiquement lors quand le composant est détruit ce qui permet de ne pas avoir à le faire sur le composant. 

#### REQUÊTES HTTP
`app.module.ts`
```js
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule {
}
```
Il recommandé de ne jamais effectuer directement des requêtes dans des classes.
Il faut utiliser un service externe qui utilisera le client **HttpClient** d’Angular.
`messageList.component.ts`
```js
export class MessageListComponent implements OnInit {
  messages: string[];

  constructor (private messageService: MessageService) {}

  ngOnInit() { this.getMessages(); }

  getMessages() {
    this.messageService.getMessages().subscribe(messages =>
	    this.messages = messages
	);
  }

  postMessage(message: string) {
    this.messageService.envoyerMessage(message).subscribe(message  =>
	    this.messages.push(message)
    );
  }
}
```
`message.service.ts`
```js
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class MessageService {

constructor(private http: HttpClient) {}

  getMessages() {
    return this.http.get(URL)
  }
  
  envoyerMessage(message: Message) {
    return this.http.post(URL, { message })
  }
}
```
Typer la réponse
`message.interface.ts`
```js
interface Message {
  auteur: string,
  date: Date,
  text: string
}
```
`message.service.ts`
```js
getMessages(): Observable<Message[]> {
   return this.http.get<Message[]>(URL)
}
```
**GET**
```js
this.http.get(url, options)
```
**POST**
```js
this.http.post(url, { message }, options)
```
**PUT**
```js
this.http.put(url, body, options)
```
**DELETE**
```js
this.http.delete(url, options)
```
###### OPTIONS
Passer un header avec un token d'authentification
```js
envoyerMessage(message: Message) {
  return this.http.post(
    URL,
    { message },
    {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
        Authorization: "my-auth-token"
      })
    }
  );
}
```
Passer des paramètres avec la méthode **set()** depuis **httpParams()** ou bien avec **formString** ou **formObject**
```js
envoyerMessage(message: Message) {
  return this.http.post(URL, { message }, {
    params: new HttpParams().set('tag', 'xx'),
  })
}
```
Exemple d'options paramétrables :
```js
get(
  url: string,
  options?: {
    headers?: HttpHeaders | { [header: string]: string | string[] };
    observe?: "body";
    params?: HttpParams | { [param: string]: string | string[] };
    reportProgress?: boolean;
    responseType?: "json";
    withCredentials?: boolean;
  }
): Observable<Object>;
```
Exemple, si nous souhaitons accéder aux **headers** de la réponse et pas uniquement au **body**, il suffit de passer à la méthode l'option **observe** et de la définir à **response** pour obtenir toute la réponse :
```js
http
  .get<MesData>(URL, {observe: 'response'})
  .subscribe(res => {
    // Le type de la réponse est alors HttpResponse<MesData>.
   // Il est ensuite possible d’accéder facilement aux headers ou au body :
    console.log(res.headers.get(NOM_HEADER));
    console.log(res.body.NOM_CHAMP);
  });
```
###### GESTION DES ERREURS
```js
getMessages() {
  this.messageService.getMessages()
    .subscribe(
                messages => this.messages = messages,
                error =>  console.log('erreur') );
}
```
Deux types d'erreurs :
- Les erreurs serveurs ( 404, 402, 500 )
- Les erreurs `ErrorEvent` ( erreurs client, erreurs réseaux )

Le type du paramètre error du callback est **HttpErrorResponse**, nous pouvons donc typer ce paramètre et distinguer les deux types d'erreur :
```js
private handleError (error: HttpErrorResponse) {
  let messageErreur: string;

  if (error.error instanceof ErrorEvent) {
    // Dans ce cas il s’agit d’une erreur côté client ou réseau.
    // messageErreur = err.error.message;
      console.log('Erreur :', error.error.message);
  } else {
    // Dans ce cas il s’agit d’une erreur serveur :
    console.log(`Erreur serveur ${error.status}, body : ${error.error}`);
  }
}
```
L'opérateur `retry` d'`rxjs`permet de réessayer une requête un nombre donné de fois, passé en paramètre
```js
import {retry, catchError } from 'rxjs/operators';

http
  .get<Message[]>(URL)
  .pipe(
      retry(2),
      catchError(this.handleError)
    );
```

###### INTERCEPTEURS
Un **intercepteur** permet de transformer une requête créée par l'application avant qu'elle ne soit envoyée au serveur, ou de transformer une réponse avant qu'elle n'atteigne l'application. Il s'agit d'une couche intermédiaire supplémentaire entre l'application et le serveur.

Exemple basique qui ne fait rien
```js
import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';

@Injectable()
export class TestInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req);
  }
}

```
**intercept** est une méthode qui transforme la requête HTTP en un Observable qui retournera la réponse.
**next** permet de passer la requête à un autre intercepteur ou au backend si il n'y a plus d'intercepteur.
Il faut l'ajouter aux **providers**
```js
import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS} from '@angular/common/http';

@NgModule({
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TestInterceptor,
    multi: true,
  }],
})
export class AppModule {}
```
**multi: true** est obligatoire et permet d'indiquer que HTTP_INTERCEPTORS est un tableau de valeur et non pas une valeur
Les intercepteurs sont appliqués selon l'ordre dans lequel ils sont **provide**

Définir un token d'authentification
```js
import {Injectable} from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest} from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Obtention du token :
    const authHeader = this.auth.getAuthorizationHeader();
    // Cloner la requête pour ajouter le header :
    const authReq = req.clone({headers: req.headers.set('Authorization', authHeader)});
    // Retourner la nouvelle requête avec le header :
    return next.handle(authReq);
  }
}
```
Parfois les applications ont besoin de quantité importantes de données et ces transferts peuvent prendre du temps. Par exemple, lors du téléchargement d'un fichier vers le serveur, il est de bonne pratique d'indiquer à l'utilisateur sa progression
Pour cela, il faut d'abord créer une requête **Http** contenant en premier paramètre le type de requête, en deuxième l'**url**, en troisième le **fichier** et en dernier un objet contenant les **options**. Dans cet objet nous avons besoin de définir **reportProgress** à **true** :
```js
const req = new HttpRequest('POST', url, fichier, {
  reportProgress: true,
});
```
Suivant que ces événement soit de type **HttpEventType**.**UploadProgress** ou **HttpResponse**, nous pouvons soit définir le pourcentage de progression pour l'afficher à l'utilisateur, soit lui indiquer que le téléchargement est bien terminé :
```js
this.http.request(req).subscribe((event: HttpEvent<any>) => {
  if (event.type === HttpEventType.UploadProgress) {
    const pourcentage = Math.round(100 * event.loaded / event.total);
    console.log(`Le téléchargement du fichier en est à ${pourcentage}%.`);
  } else if (event instanceof HttpResponse) {
    console.log('Le fichier est téléchargé sur le serveur');
  }
})
```


#### MODULES
**forRoot()** déclare les routes à la racine
**forChild()** déclare les routes dans les modules enfants
**Lazy Loading** permet de charger les modules selon les routes où l'on est et non de les charger tous en même temps au moment où ce lance l'application. Cela rend l'application plus performante.

`app.routes.ts`
```js
export const APP_ROUTES: Routes = [
  { path: "", redirectTo: "cocktails", pathMatch: "full" },
  {
    path: "cocktails",
    loadChildren: () =>
      import("./features/cocktail/cocktail.module").then(m => m.CocktailModule)
  }
];
```
`import('path...')` est un import dynamique
les **lazy modules** sont chargés grace à la clé **loadChildren**

`cocktail.routes.ts`
```js
export const COCKTAIL_ROUTES: Routes = [
  {
    path: "",
    component: CocktailContainerComponent,
    children: [
      { path: "new", component: CocktailFormComponent },
      { path: ":index/edit", component: CocktailFormComponent },

      { path: ":index", component: CocktailDetailsComponent },
      { path: "", redirectTo: "0", pathMatch: "full" }
    ]
  }
];
```
**path** est désormais égale à **""**

#### BUILD
l'option **outputPath** dans `angular.json` permet de changer la destination du **build**

La commande **build** va :
- utiliser la compilation AOT (Ahead-of-Time Compilation) pour pré-compiler les templates
- activer le mode production (désactivation de toutes les fonctionnalités de développement)
- utiliser Webpack pour rassembler les fichiers en bundles
- effectuer la minification du code (suppression des espaces surnuméraires des commentaires et caractères optionnels)
- effectuer l'obfuscation (réécriture du nom des variables et des noms de fonction pour les réduire et les rendre cryptique)
- enlever le code non utilisé.

**ng build --watch** recompile l'application à chaque changement
###### CONFIGURATION DES FICHIERS D'ENVIRONNEMENT
`src/environnement/environnement.ts`
`src/environnement/environnement.prod.ts`
`src/environnement/environnement.staging.ts`

exemple
```js
export const environment = {
  apiUrl: 'http://api-developement'
};
```

```js
import { environment } from './../environments/environment';
// …
console.log(environment.apiUrl);
```
`angular.json`
```js
"configurations": {
  "production": {
    "budgets": [
      {
        "type": "initial",
        "maximumWarning": "500kb",
        "maximumError": "1mb"
      },
      {
        "type": "anyComponentStyle",
        "maximumWarning": "2kb",
        "maximumError": "4kb"
      }
    ],
    "fileReplacements": [
      {
        "replace": "src/environments/environment.ts",
        "with": "src/environments/environment.prod.ts"
      }
    ],
    "outputHashing": "all"
  },
  "development": {
    "buildOptimizer": false,
    "optimization": false,
    "vendorChunk": true,
    "extractLicenses": false,
    "sourceMap": true,
    "namedChunks": true
  }
},
"defaultConfiguration": "production"
```
Il faut ajouter l'option **fileReplacements** pour chaque environnement où c'est nécessaire.
Par exemple ici, ici dans la **configuration** pour l'environnement de **production**.
Si nous l'ajoutions pour **staging**, nous mettrions également :
```js
"configurations": {
  "production": { … },
  "staging": {
    "fileReplacements": [
      {
        "replace": "src/environments/environment.ts",
        "with": "src/environments/environment.staging.ts"
      }
    ]
  }
}
```
Pour construire pour un environnement autre que celui de **production** il faut le spécifier en le passant en argument à la commande de **build** :
`ng build --configuration=staging`

#### ANIMATIONS
import minimum requis dans les modules voulus
```js
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
```
les ajouter dans la partie import des modules
```js
@NgModule({
  ...
  imports: [
    BrowserModule,
    BrowserAnimationsModule
  ],
})
```
puis les imports nécessaires dans les composants
```js
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';
```
###### TRIGGER, STATE ET STYLE
ajouter la propriété **animation**
```js
@Component({
  selector: 'app-exemple',
  templateUrl: 'app-exemple.component.html',
  animations: [
    trigger('nomDuTrigger', [
      state('actif', style({ opacity: 1 })),
      state('desactive', style({ opacity: 0 })),
      transition('actif => desactive', [
        animate("1s")
      ])
    ])
  ]
})
```
`mon_composant.component.html`
```html
<div [@nomDuTrigger]="expression">...</div>
```
###### TRANSITION
```js
function transition(stateChangeExpr: string, steps: AnimationMetadata | AnimationMetadata[], options: AnimationOptions | null): AnimationTransitionMetadata;
```

```js
transition('actif => desactive', animate(500)),
```
###### KEYFRAMES
```js
animate("5s", keyframes([
  style({ backgroundColor: "yellow", offset: 0 }),
  style({ backgroundColor: "blue", offset: 0.1 }),
  style({ backgroundColor: "purple", offset: 0.4 }),
  style({ backgroundColor: "black", offset: 1 })
])
```
###### GROUPS
**groups** déclenche l'animation de différentes propriétés en même temps mais sur des durées différentes
```js
group([
  animate(200, style({ borderRadius: "50%" })),
  animate(2000, style({ backgroundColor: "red" }))
])
```
###### QUERY ET STAGGER
**query** va permettre de selectionner plusieurs éléments ( des enfants du **trigger** )
**stagger** va permettre de séparer l'animation de chacun des éléments par un intervale de temps
```js
animations: [
  trigger("list", [
    transition(
      ":enter",
      query("li", [
        style({ opacity: 0, transform: "translateX(-10px)" }),
        stagger(50, animate(400))
      ])
    )
  ])
]
```
###### START ET DONE
**start** et **done** sont des hooks que l'on va pouvoir utiliser côtés template
```js
<p (@nomTrigger.start)="debutAnimation($event)"
  (@nomTrigger.done)="finAnimation($event)"
  [@nomTrigger]="'actif'">
</p>
```
le type de l'évènement est **animationEvent**
```js
interface AnimationEvent {
  fromState: string
  toState: string
  totalTime: number
  phaseName: string
  element: any
  triggerName: string
  disabled: boolean
}
```
#### TESTS
```js
describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });
```
**describe** est une fonction de **Jasmine**
**beforeEach** reinitialize **TestBed** avant d'éxécuter le prochain test
**compileComponents** compile le composant à tester

exemple
```js
describe('XComponent - template inline', () => {

  let comp:     XComponent;
  let fixture:  ComponentFixture<XComponent>;
  let debug:    DebugElement;
  let el:       HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ XComponent ],
    });

    fixture = TestBed.createComponent(XComponent);

    debug = fixture.debugElement.query(By.css('h1'));
    el = debug.nativeElement;
  });
});
```
**detectChanges()** permet de d'identifier des changements dans le composant car en fonction des cycles de vie, il faut tester au bon moment.
Si une variable est appellé dans le template par interpolation, il faut être sûre que la variable est chargé avant le test
```js
it('devrait afficher le titre ', () => {
  fixture.detectChanges();
  expect(el.textContent).toContain(fixture.componentInstance.titre);
});
```
###### COMPOSANT AVEC DÉPENDANCES
Les **stub** ( ou **mock** ) sont une réplique des services qui retourne des données minimales pour tester un composant dépendant de ce services
```js
class MockUserService {
  connected: true,
  user: { prenom: 'Jean', nom: 'Dupont'}
}
```
On provide le **service** ( qui n'est pas le vrai service ) en lui attribuant la valeur du **mock** grâce à la clé **useClass**
```js
TestBed.configureTestingModule({
   declarations: [ XComponent ],
   providers:    [ {provide: UserService, useClass: MockUserService } ]
});
```
Pour accéder dans les tests au **mock** du service, il est recommandé d'utiliser l'**injecteur** de **TestBed**
```js
beforeEach(() => {

  TestBed.configureTestingModule({
     declarations: [ XComponent ],
     providers:    [ {provide: UserService, useClass: MockUserService } ]
  });

  fixture = TestBed.createComponent(XComponent);

  userService = TestBed.inject(UserService);
```
On peut ainsi accéder au **mock** dans une **spec**
```js
it('doit afficher Paul', () => {
  userService.user.name = 'Paul';
  fixture.detectChanges();
  expect(el.textContent).toContain('Paul');
});
```
Une autre manière de tester les composants sans attendre la valeur de retour asynchrone de services est d'utiliser des **spy**
Un **spy** permet de simules le retour d'une requête asynchrone
Contrairement à l'utilisation d'un **stub**, pour utiliser un **spy** nous injectons le véritable **service** dans l'instance du composant testé
**createSpyObj** est une fonction de Jasmine qui permet de remplacer une méthode d'un objet lorsque celle-ci est appelée
**createSpyObj** prend donc deux paramètres : premièrement l'objet sur lequel le spy est installé et deuxièmement un tableau contenant les noms des méthodes à remplacer avec ce spy
```js
httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
…
httpClientSpy.get.and.returnValue(asyncError(errorResponse));
```
Nous utilisons le **spy** sur une instance du véritable **service** **userService** et lorsque que la méthode **getUser()** est appelée celui ci permet de retourner immédiatement une promesse résolue avec la valeur du **user** que nous souhaitons
```js
it('should return expected values', () => {
  const expectedData = [{ id: 1, name: 'A' }, { id: 2, name: 'B' }];

  httpClientSpy.get.and.returnValue(asyncData(expectedData));

  userService.getData().subscribe(
    users => expect(users).toEqual(expectedData, 'expected users'),
    fail
  );
  expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
});
```
##### TESTS ISOLÉS
Les **tests isolés** permettent de tester les partie de l’application qui sont isolées, c’est-à-dire qui peuvent fonctionner **sans interaction avec d’autres éléments Angular**

Un pipe
```js
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'titlecase', pure: false})

export class TitleCasePipe implements PipeTransform {
  transform(input: string): string {
    return input.length === 0 ? '' :
      input.replace(/wS*/g, (txt => txt[0].toUpperCase() + txt.substr(1).toLowerCase() ));
  }
}
```
Un test isolé
```js
describe('TitleCasePipe', () => {

  let pipe = new TitleCasePipe();

  it('transforme 'paul' en 'Paul'', () => {
    expect(pipe.transform('paul')).toBe('Paul');
  });

});
```
