
### INSTALLATION

install une base mongo local
`pamac build mongodb-bin`

install des outils mongo pour faire des backup de la base ( mongodump, mongorestore, ... )
`pamac build mongodb-tools`

install compass, outil de gestion de base de donnée en interface graphique
`pamac build mongodb-compass`

###### extension MongoDB pour Codium
MongoDB for VS Code permet de éxécuter des opération sur la base de donnée via Codium ( cliquer sur la nouvelle icone de la feuille et créer un playground )

---
### SERVICE

lancer les service mongo
`systemctl start mongodb.service`

stop service
`systemctl stop mongodb.service`

status du service
`systemctl status mongodb.service`

---
### SHELL MONGO

run shell
`mongo`

déclarer un id
`_id: ObjectId('61ed3710c8ccf081d6e7c9b7')`

```
db.users.insertMany([
  { name: "Lola" },
  { name: "Toma" },
  { name: "Conrad", _id: ObjectId('61ed3710c8ccf081d6e7c9b7')},
  { name: "Mateo" }
], {ordered: false})
```
si une erreur surviens lors de l'enregistrement de donnée, il passe outre et enregistrera les données suivantes


create database
`use ma_database`

supprime une base de donnée ( "switcher" dessus au préalable )
`db.dropDatabase()`

drop la collection toto
`db.toto.drop()`

créé un utilisateur ( attention de respecter le model à l'intérieur, voir la doc de mongodb )
`db.createUser({...})`

create collection
`db.createCollection('ma_collection')`

insert new data in collection
`db.ma_collection.insertOne({ 'name': 'edouard })`

récupère la date de création du document ( s'éxécute sur un ObjectId() )
`<_id>.getTimestamp()`

affiche toutes les méthodes du document ( db.users.find() est un cursor alors que db.users.findOne() est un document )
`var cursor = db.users.find()`

unknown
`printjson(cursor)`

pretty affiche l'arborescence du json sur plusieurs lignes
`db.users.find().pretty()`

path for config file
`/etc/mongodb.conf`