
installer la dernière version d'npm
`npm install npm@latest -g`

Lancer un projet react
```
npx create-react-app my-app
cd my-app
npm start
```

ou
`mkdir <mon_projet>`
créer un json
`npm init`

définir les dépendences nécessaires dans le json:
```
{
  "name": "visio",
  "version": "1.0.0",
  "dependencies": {
    "@material-ui/core": "^3.3.1",
    "@material-ui/icons": "^3.0.1",
    "react": "^16.6.0",
    "react-dom": "^16.6.0",
    "react-scripts": "2.0.5"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
}
```

```
public/
└── index.html
src/
├── index.js
└── main.js
```

ne pas oublier d'importer dans index.js :
```
import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './main';
```


`npm start`

exporter une Fonction pour la tester dans la console
`export function sum() {.....}`
`export * from './sum'`
`import { sum } from './components'`
`window.sum = sum`	ou	`global.sum = sum`

mise à jour des dépendences
`npm i npm-check-updates`
`ncu -u`

install les dépendances aux regard de package.json
`npm install`

observe la version actuelle des dépendences
`npm view <nom de la dependence>`
