###### BASIQUES

|syntaxe|description|
|---|---|
|\||pour enchainer les commandes. ex : history \| grep npm|
|!1997|éxécution d'une commande dans l'historique avec son numero|
|cat -n \<file>|affiche les numéros de lignes du fichier|
|cp|copier/coller|
|cp -r|copier/coller un dossier|
|ctrl c|interompre une commande en cours|
|ctrl r|rappel historique|
|ctrl k|coupe la ligne du curseur jusqu'à la fin|
|ctrl u|coupe la ligne du début jusqu'au curseur|
|ctrl w|supprime un mot aprés l'autre|
|ctrl y|copie la dernière ligne mise en cache|
|cp -v|copie/renomme|
|ctrl shift 0|taille du texte par default|
|var=3|créé une variable|
|$var|execute la variable|
|echo $var|affiche la variable|
|$var >> toto.txt|injecte le résultat de la variable dans un fichier|

---
###### .&!>
éxécute un fichier.sh
`./filename`

& rend la main dans la console aprés ouverture de l'application
`npm start &`

vider un fichier de son contenu
`> fichier.txt`

###### A
liste les alias
`alias`

créer un alias
`alias [new_alias]='[new_commande]'`

remove alias
`unalias [alias-name]`

garde une commande dans l'historique même si je ne l'ai pas éxécuter
`Alt + Alt Gr + #`

chercher un packet
`apt search <python3-pytest>`

###### B
controler le bluetooth
`bluetoothctl`

###### C
connaître le type de lecteur disponible sur mon pc	(si 0 → SSD	si 1 → HDD)
`cat /sys/block/sda/queue/rotational`

redéfinie le propriétaire du fichier en tant qu'edouard
`chown -R edouard [FileName]`

change console
`ctrl alt <F-key>`

Pour revenir à la console par default [ctrl alt \<F-key>]
`ctrl alt F7`

injecte le contenu d'un fichier dans un autre ( écrase )
`cat toto > tata`

###### D
liste les espaces disques
`df`

diff entre 2 fichiers
`diff -u toto tata`

créé un nouveau fichier avec la diff que je peut éditer dans atom
`diff -u toto tata new_toto`

met à jour la version de xubuntu (`sudo`)
`do-release-upgrade`

connaître la source du fichier ( son packet d'origine )
`dpkg -S pytest3`

list les packets installé sur ma distrib
`dpkg -l <package name>`

supprimer un packet sur la distrib
`dpkg -r <package name>`

supprimer un packet et sa config
`dpkg -P <package name>`

installation d'un packet `.deb` (`sudo`)
`dpkg -i package_file.deb`

liste l'ensemble des paquets installés
`dpkg --get-selections`

cherche les packets disponible sur internet
`dpkg --search <nom du packet ou mot associé>`

liste tous les fichiers et sous fichiers dans un répertoire
`du`

liste les dossiers les plus gros sur le disques
`du -hsx * | sort -rh | head -5`

vérifier la taille d'un fichier ou d'un dossier
`du -s \<fileName or folderName>`

###### E
show id process of the current running process
`echo $$`

affiche la variable PATH (joliement)
`echo $PATH | tr : \\n`

affiche les variables d'environnement
`env`

affiche la longueur de ma chaîne de caractère
`expr length <ma chaîne de caractère>`

###### F
liste l'ensemble des disques (`sudo`)
`fdisk -l`

ramène au premier plan un process en cours ( comme vi )
`fg`

connaître l'extension d'un fichier
`file`

ouvre une image au hazard comprise entre 0 et 90
`fim yoshimasa/Komi_Komi_Pako_Pako_2/$(( $RANDOM % 90 )).jpg`

trouve le nom dans l'arbre des dossiers
`find -name "zephir-compose"`

compte le nombre de fichier dans un répertoire
`find . -type f -iname "*.*" | wc -l`

créer un fichier avec le nombre de fichier par dossier ( ou quelque-chose dans le genre )
`find . -name '*.*' | perl -nle 's/^.*\.//; ++$n{$_}; END { foreach (sort { $n{$a} <=> $n{$b} || $a cmp $b } keys(%n)) { print "$n{$_},$_" } }' > result.txt`

file transfer protocol
`ftp`

remplace toutes les occurences "toto" par "tata" dans tous les fichiers .jpg
`for file in *.jpg; do mv $file ${file/toto/tata}; done`

remplace tous les \<fichiers>.jpg par <new_name>+<number_incrément>
`for file in *.jpg; do mv $file ${file/*/toto_$((++number))}; done`

passe le résultat d'une commande à une variable et éxécute une nouvelle commande sur la variable
`toto=$(for i in {120..125}; do echo femme_$i.jpg; done;) & fim $toto`

injecte le résultat d'une commande dans un fichier
`toto=$(tree -f | grep karma.conf.js) | echo $dd > fichier.txt`

###### G
retrouve un mot clef dans l'historique, les fichier etc...1
`grep`

exclure un dossier lors de la recherche de patterne
`grep -r --exclude-dir=venv <pattern>`

###### H
show 50 first lines of file
`head -n50 <fichier>`

historique des commandes
`history`

###### I

connaitre les interface réseaux
`ifconfig`

surligner les IP des interfaces réseaux
`ifconfig | egrep "([0-9]{1,3}\.){3}[0-9]{1,3}|$"`


indique toutes les adresses réseaux aux-quelles je suis connecté
[ip-la-commande-linux-pour-gerer-son-interface-reseau](https://memo-linux.com/ip-la-commande-linux-pour-gerer-son-interface-reseau/)
`ip a (addr)`

connaître son adresse IP sur toutes les interfaces réseau.	( -4 affiche uniquement addresse ipv4 | -o affiche resultat sur une seule ligne | -c affiche en couleur  )
`ip -4 -o -c addr`

unknown
`iptables-save`

donne toutes les infos sur sa machine ( selon les options passées en argument )
`inxi`

afficher | configurer les appareils sans fil
`iw <arg>`

###### J
injecte le résultat dans un fichier ( cat toto.txt affiche l'image )
`jp2a --colors camilla.jpg >> toto.txt`

exécuter un fichier java
`java -jar <fichier>.jar`

###### K
kill suivi du numéro du process pour stopper le process
`kill <process_id>`

###### L
créer un lien physique
`ln source dest`

créer un lien symbolique				
`ln -s source dest`

créer un lien symbolique dans un autre répertoire qui pointe vers le fichier (`sudo`)
`ln -s /repertoire/site1.conf /autre-repertoire/`

cherche dans la liste de updatedb
`locate`

supprime toutes les fichiers qui correspondent au pattern
`ls | grep -P "[0-9]{2}_" | xargs -d"\n" rm`

info sur ma distribution tel que la version d'ubuntu
`lsb_release -a`

quelle processus utilise le fichier ( utile quand il refuse d'éjecter la clé usb )
`lsof`

###### M
obtenir des infos sur une commande
`man <nom de commande>`

créer un dossier
`mkdir`

créer un dossier et monte la cré dessus (`sudo`)
`mount path/to/target_directory --mkdir path/to/device_file`

renommer
`mv <old name> <new name>`

couper/coller
`mv <source> <destination>`

###### N
check open ports on linux
`netstat`

unknown
`netstat -tupln`

lister le périférique wifi disponible
`nmcli c`

se connecter à l'un des périfériques ( `c` pour cennection )
`nmcli c up <DEVICE name>`

se déconnecter à l'un des périfériques ( `d` pour device )
`nmcli d disconnect <DEVICE name>`

controler les connection wifi via interface graphique
`nmtui`

###### P
retrouver le process d'un logiciel
`pgrep <nom du logiciel>`

tester la connexion au serveur et connaître l'adresse ip du serveur
`ping <nom du serveur>`

Arrête le logiciel
`pkill <n° du process> ou <nom du logiciel>`

liste les process en cours
`ps faux`

check CPU usage for a process
`ps aux | grep nginx`

###### R
réinitialise la console
`reset`

recherche récursive d'une chaîne de charactère dans les fichiers et dossiers
`rgrep`

supprime les fichiers
`rm`

suppression récurssive (qui descend dans l'arbre d'un dossier pour voir tous les éléments )
`rm -r`

force la suppression total du dossier
`rm -r f`

synchronise des dossiers ( évite les copier/coller de sauvegarde )
`rsync -r`

###### S
copy from server
`scp <File> <destination>`

copy ssh
`scp keycloak-theme/ root@192.168.0.28:/home/zephir/`

copy du zephir2 vers le container depuis le container 
`scp -r 192.168.0.28:/home/zephir/keycloak-theme ./`

remplace tout les mots-clé dans un fichier
`sed -i 's/old-text/new-text/g' input.txt`

unknown
`service udev status`

installer un fichier.sh
`sh FileName.sh`

se connecter à une machine distante via le protocole ssh
`ssh -X root@zephir2.ac-test.fr`

copier sa clé publique sur une machine pour pouvoir se connecter sans s'authentifier.
`ssh-copy-id pi@192.168.230.193`

pour générer une clé publique. ( besoin d'OpenSSH )
`ssh-keygen`

modifie les paramètres d'interface
`stty`

se connecter en tant que root
`sudo -s`

rappel de la dernière commande
`sudo !!`

vérifie le status d'un demon
`systemctl status udev`

###### T
dépaqueter un fichier `tar.gz`
`tar -xzvf <fichier>.tar.gz`

dépaqueter un fichier `tar.bz2`
`tar -xf <fichier>.tar.bz2`

change heure fuseaux horaires
`timedatectl`

créer un fichier
`touch`

créé plusieurs fichier toto01.txt
`touch toto{01..10}.txt`

affiche les processus
`top`

dessine un arbre de tous les dossiers et sous dossiers depuis le chemin courant
`tree`

affiche tous les fichiers présent dans l'aborescence se terminant par .tests.js
`tree -P *.tests.js`

compte le nombre de fichier dans un répertoire
`tree | tail -1`

tree only files
`tree -FL 1 | grep -v /$`

supprime le contenu d'un fichier ( -s pour la taille )
`truncate -s 0 toto.txt`

###### U
uncomplicated fire wall
`ufw`

affiche toutes les infos concernant l'administrateur et la machine
`uname -a`
`uname -v`
`uname -r`

met à jour la liste de tous les chemins possibles sur la machine (`sudo`)
`updatedb`

###### W
affiche le nombre de lignes
`wc -l <nom_du_fichier>`

affiche le nombre de fichier et de dossier dans le dossier courant
`ls | wc -l`

affiche le nombre de bytes ( ex :  wc -c \<file name> )
`wc -c <nom_du_fichier>`

affiche le nombre de caractères
`wc -m <nom_du_fichier>`

indique la longueur de la plus longue ligne
`wc -L <nom_du_fichier>`

affiche le nombre de mots
`wc -w <nom_du_fichier>`

télécharger un fichier depuis une url
`wget <url>`

récupère toute les images de 1 à 236 depuis une url
`wget -nd -H -p -A jpg,jpeg,png,gif -e robots=off https://un_site.com/g/296050/{1..236}/`

connaître le débit internet sur un site
`wget -O /dev/null <adresse du site>`

connaître l'emplacement d'un executable bin
`which <bin_script>`

install wine ( installation d'executable windows sur linux )
`wine [FileName.exe]`

régénère un dossier de config .wine à la racine (penser à le supprimer avant de le régénérer)
`winecfg`

---
###### historique autocomplétion
[raccourci_pour_completer_les_commandes_a_partir_de_l'historique](https://doc.ubuntu-fr.org/terminal#pour_completer_les_commandes_a_partir_de_l'historique)

Dans votre fichier **~/.inputrc**, ajoutez à la fin :

```sh
"\e[A": history-search-backward
"\e[B": history-search-forward
```

Dans votre fichier **~/.bashrc**, ajoutez à la fin :

```sh
shopt -s histappend
PROMPT_COMMAND='history -a'
```

---
#### CHEMIN UTILES
fonts system
`/usr/share/fonts`

fonts ajoutée
`~/.locale/share/fonts`

luminosité
`/sys/class/backlight/amdgpu_bl1/brightness`