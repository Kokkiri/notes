installation
`sudo apt install xclip`

le presse papier par defaut est **primary** ( click molette milieu )
`echo "Du texte en presse papier" | xclip`

copier le contenu d'un fichier
`xclip < mon_fichiers.txt`

changer de presse papier
`date | xclip -selection clipboard`
