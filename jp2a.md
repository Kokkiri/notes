**converti les image en ASCII**

`jp2a --chars='  ' --fill --color --html femme_125.jpg > toto.html`
- `--chars='  '` supprime les caracteres
- `--fill` colore l'arrière plan
- `--html` créé une sortie html

combiner avec Imagik
`convert shapes.svg jpg:- | jp2a - --width=80`
- `convert` converti le format du fichier