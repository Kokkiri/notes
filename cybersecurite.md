###### Outils

- ExploitDB
- [Exegol](https://exegol.readthedocs.io/en/latest/) installation de tous les outils professionnels via `pip` ( une alternative à KaliLinux )
- Searchsploit
- KVM
- Magento
- Nmap
- Hydra
- Gobuster
- Dirbuster
- Virtualbox
- Bitburner (jeux steam)
- Légion
- Rcrack
- Pfsense
- Ethercap
- analyse FORENSIQUE ( analyse d'un système après une attaque )
- cherrytree ( prise de note )
- [Flameshot](https://flameshot.org/) ( outil de screenshot )
- [Ghostwriter](https://github.com/GhostManager/Ghostwriter) outil de rapport pour pentest
- [Pwndoc](https://github.com/pwndoc/pwndoc) outil de rapport pour pentest
- [Peek](https://github.com/phw/peek) enregistrement gif

###### Ressource
- Firewall (connaissance)
- Réseau (connaissance)
- Hackfest (festival de hacking)
- Offense.security
- [thehacker.recipes](https://www.thehacker.recipes/) techniques de hacker
- [hacktricks](https://book.hacktricks.xyz/welcome/readme) techniques de hacker
- [ired.team](https://www.ired.team/) notes à propos de *red teaming* et *offensive security*.

###### Entrainement, Formation et Certification
[OffSec](https://www.offsec.com/) FC *délivre une certification OSCP (Offensive Security Certified Professional)*
[eJPT](https://www.kali-linux.fr/conseil/certification-ejpt-tout-ce-quil-vous-faut-pour-lobtenir) C
[HTB academy](https://academy.hackthebox.com/) FC
[TryHackMe](https://tryhackme.com/) E
[CEH](https://www.devensys.com/formations/certified-ethical-hacker) C
[Kalitraining](https://kali.training/) E *par OffSec*

# MISE EN PLACE
## PRÉPARATION

Si nous organisons notre structure en fonction des étapes des tests de pénétration et du système d'exploitation des cibles, un exemple de structure de dossier pourrait ressembler à ce qui suit.

```session shell
tchadoyeon@htb[/htb]$ tree .

.
└── Test de pénétration
	│
	├─── Pre-Engagement
	│ └── ...
    ├── Linux
    │ ├─── Collecte d'informations
    │ │ └── ...
    │ ├──── Vulnerability-Assessment
    │ │ └── ...
    │ ├─── Exploitation
    │ │ └── ...
    │ ├─── Post-Exploitation
    │ │ └── ...
    │ └─── Lateral-Movement
    │ └── ...
    ├── Windows
    │ ├─── Collecte d'informations
    │ │ └── ...
    │ ├──── Vulnerability-Assessment
    │ │ └── ...
    │ ├─── Exploitation
    │ │ └── ...
    │ ├─── Post-Exploitation
    │ │ └── ...
    │ └─── Lateral-Movement
    │ └── ...
    ├─── Reporting
    │ └── ...
	└─── Résultats
	    └── ...
```

Si nous sommes spécialisés dans des domaines spécifiques du test d'intrusion, nous pouvons bien sûr réorganiser la structure en fonction de ces domaines. Nous sommes tous libres de développer un système qui nous est familier, et il est d'ailleurs recommandé de le faire. Chacun travaille différemment et a ses forces et ses faiblesses. Si nous travaillons en équipe, nous devrions développer une structure que chaque membre de l'équipe connaît. Prenez cet exemple comme point de départ pour créer votre système.

```session shell
tchadoyeon@htb[/htb]$ tree .

.
└─── Penetration-Testing
	│
	├─── Pre-Engagement
	│ └── ...
    ├─── Network-Pentesting
	│ ├─── Linux
	│ │ ├─ Information-Gathering
	│ │ │ └── ...
	│ │ ├─── Vulnerability-Assessment
    │ │ │ └── ...
    │ │ └── ...
    │ │ └── ...
    │ ├─── Windows
    │ │ ├─ Information-Gathering
    │ │ │ └── ...
    │ │ └── ...
    │ └── ...
    ├── WebApp-Pentesting
	│ └── ...
    ├── Social-Engineering
	│ └── ...
    ├── .......
	│ └── ...
    ├─── Reporting
    │ └── ...
	└─── Résultats
	    └── ...
```

### Gestionnaire de mots de passe

Un autre élément essentiel pour nous est le gestionnaire de mots de passe. Les gestionnaires de mots de passe peuvent s'avérer utiles non seulement à des fins personnelles, mais aussi pour les tests de pénétration. L'une des vulnérabilités ou méthodes d'attaque les plus courantes au sein d'un réseau est la "réutilisation des mots de passe". Nous essayons d'utiliser des mots de passe et des noms d'utilisateur trouvés ou décryptés pour nous connecter à plusieurs systèmes et services au sein du réseau de l'entreprise par le biais de cette attaque. Il est encore assez courant de trouver des identifiants qui peuvent être utilisés pour accéder à plusieurs services ou serveurs, ce qui nous facilite le travail et nous offre plus d'opportunités d'attaque. Les mots de passe ne posent que trois problèmes :

1. **La complexité**
2. **Réutilisation**
3. **Souvenir**

Les **gestionnaires de mots de passe** résolvent tous les problèmes mentionnés ci-dessus, non seulement pour les utilisateurs standard, mais aussi pour les testeurs de pénétration. Nous travaillons avec des dizaines, voire des centaines, de services et de serveurs différents pour lesquels nous avons besoin d'un mot de passe nouveau, fort et complexe à chaque fois. Parmi les fournisseurs, on peut citer les suivants, sans toutefois s'y limiter :

|**[1Password](https://1password.com/)**|**[LastPass](https://www.lastpass.com/)**|**[Keeper](https://www.keepersecurity.com/)**|**[Bitwarden](https://bitwarden.com/)**|**[Keypass](https://keepass.info/)**|
|---|---|---|---|---|

#### Utile
transférer les modules firefox grâce à la création d'un compte firefox.
création de script pour réinitialiser une machine sans avoir à tout réinstaller à la main.

#### Enregistrement

L'enregistrement est essentiel à la fois pour la documentation et pour notre protection. Si des tiers attaquent l'entreprise pendant notre test de pénétration et que des dommages surviennent, nous pouvons prouver que ces dommages ne résultent pas de nos activités. Pour cela, nous pouvons utiliser les outils **script** et **date**. **Date** peut être utilisé pour afficher la date et l'heure exactes de chaque commande dans notre ligne de commande. Avec l'aide de **script**, chaque commande et le résultat qui en découle sont sauvegardés dans un fichier d'arrière-plan. Pour afficher la date et l'heure, nous pouvons remplacer la variable **PS1** dans notre fichier **.bashrc** par le contenu suivant.

#### PS1

```bash
PS1="\[\033[1;32m\]\342\224\200\$([[ \$(/opt/vpnbash.sh) == *\"10.\"* ]] && echo \"[\[\033[1;34m\]\$(/opt/vpnserver.sh)\[\033[1;32m\]]\342\224\200[\[\033[1;37m\]\$(/opt/vpnbash.sh)\[\033[1;32m\]]\342\224\200\")[\[\033[1;37m\]\u\[\033[01;32m\]@\[\033[01;34m\]\h\[\033[1;32m\]]\342\224\200[\[\033[1;37m\]\w\[\033[1;32m\]]\n\[\033[1;32m\]\342\224\224\342\224\200\342\224\200\342\225\274 [\[\e[01;33m\]$(date +%D-%r)\[\e[01;32m\]]\\$ \[\e[0m\]"
```
#### Date

```shell-session
─[eu-academy-1]─[10.10.14.2]─[Cry0l1t3@htb]─[~]
└──╼ [03/21/21-01:45:04 PM]$
```

Pour démarrer l'enregistrement avec **script** (pour Linux) et [Start-Transcript](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.host/start-transcript?view=powershell-7.1) (pour Windows), nous pouvons utiliser la commande suivante et la renommer en fonction de nos besoins. Il est recommandé de définir un certain format à l'avance après avoir sauvegardé les journaux individuels. Une option consiste à utiliser le format **date_heure-de-démarrage_nom.log**.
#### Script

```session shell
tchadoyeon@htb[/htb]$ script 03-21-2021-0200pm-exploitation.log

tchadoyeon@htb[/htb]$ ...SNIP...
tchadoyeon@htb[/htb]$ exit
```
#### Start-Transcript

```Session Powershell
C:\N- Start-Transcript -Path "C:\NPentesting\N03-21-2021-0200pm-exploitation.log"

La transcription a démarré, le fichier de sortie est C:\NPentesting\03-21-2021-0200pm-exploitation.log

C:\N- ...SNIP...
C:\N- Stop-Transcript
```

Cela triera automatiquement nos journaux dans le bon ordre, et nous n'aurons plus à les examiner manuellement. Cela permet également aux membres de notre équipe de comprendre plus facilement quelles mesures ont été prises et à quel moment.

Un autre avantage important est que nous pouvons analyser ultérieurement notre approche afin d'optimiser notre processus. Si nous répétons une ou deux étapes à plusieurs reprises et que nous les utilisons en combinaison, il peut être intéressant d'examiner ces étapes à l'aide d'un simple script pour gagner du temps.

En outre, la plupart des outils offrent la possibilité d'enregistrer les résultats dans des fichiers distincts. Il est fortement recommandé de toujours utiliser ces fonctions car les résultats peuvent également changer. Par conséquent, si des résultats spécifiques semblent avoir changé, nous pouvons comparer les résultats actuels avec les précédents. Il existe également des émulateurs de terminal, tels que [Tmux](https://github.com/tmux/tmux/wiki) et [Terminator](https://terminator-gtk3.readthedocs.io/en/latest/), qui permettent, entre autres, d'enregistrer automatiquement toutes les commandes et sorties. Si nous rencontrons un outil qui ne nous permet pas d'enregistrer la sortie, nous pouvons travailler avec des redirections et le programme tee. Voici à quoi cela ressemblerait :
#### Redirection de la sortie Linux

```session shell
tchadoyeon@htb[/htb]$ ./custom-tool.py 10.129.28.119 > logs.custom-tool
```

```session shell
tchadoyeon@htb[/htb]$ ./custom-tool.py 10.129.28.119 | tee -a logs.custom-tool
```
#### Redirection de la sortie Windows

```Session Powershell
C:\N- .\Ncustom-tool.ps1 10.129.28.119 > logs.custom-tool
```

```Session PowerShell
C:\N- .\Ncustom-tool.ps1 10.129.28.119 | Out-File -Append logs.custom-tool
```

## VIRTUALISATION

La virtualisation est une abstraction des ressources informatiques physiques. Les composants matériels et logiciels peuvent être abstraits. Un composant informatique créé dans le cadre de la virtualisation est appelé composant virtuel ou logique et peut être utilisé exactement comme son équivalent physique. Le principal avantage de la virtualisation est la couche d'abstraction entre la ressource physique et l'image virtuelle. C'est la base de divers services en nuage, qui prennent de plus en plus d'importance dans la vie quotidienne des entreprises. La virtualisation doit être distinguée des concepts de simulation et d'émulation.

La virtualisation implique l'abstraction des ressources informatiques physiques telles que le matériel, les logiciels, le stockage et les composants réseau. L'objectif est de rendre ces ressources disponibles à un niveau virtuel et de les distribuer à différents clients d'une manière aussi flexible qu'orientée vers la demande.  Cela devrait permettre d'améliorer l'utilisation des ressources informatiques. L'objectif est d'exécuter des applications sur un système qui n'est pas supporté par celui-ci. En matière de virtualisation, on distingue

- la virtualisation du matériel

- la virtualisation des applications

- la virtualisation du stockage

- la virtualisation des données

- la virtualisation du réseau.


La virtualisation du matériel concerne les technologies qui permettent aux composants matériels d'être disponibles indépendamment de leur base physique à l'aide d'un logiciel [hyperviseur](https://en.wikipedia.org/wiki/Hypervisor). L'exemple le plus connu est la **machine virtuelle** (**VM**). Une VM est un ordinateur virtuel qui se comporte comme un ordinateur physique, y compris le matériel et le système d'exploitation. Les machines virtuelles s'exécutent en tant que systèmes invités virtuels sur un ou plusieurs systèmes physiques appelés "hôtes".
### Machines virtuelles

Une **machine virtuelle** (**VM**) est un système d'exploitation virtuel qui s'exécute sur un système hôte (un système informatique physique réel). Plusieurs VM isolées les unes des autres peuvent fonctionner en parallèle. Les ressources matérielles physiques du système hôte sont allouées par l'intermédiaire d'hyperviseurs. Il s'agit d'un environnement virtualisé fermé, dans lequel plusieurs systèmes invités peuvent être exploités en parallèle, indépendamment du système d'exploitation, sur un ordinateur physique. Les machines virtuelles agissent indépendamment les unes des autres et ne s'influencent pas mutuellement. Un hyperviseur gère les ressources matérielles et, du point de vue de la machine virtuelle, la puissance de calcul, la mémoire vive, la capacité du disque dur et les connexions réseau qui lui sont allouées sont exclusivement disponibles.

Du point de vue de l'application, un système d'exploitation installé dans la VM se comporte comme s'il était installé directement sur le matériel. Les applications et le système d'exploitation ne savent pas qu'ils fonctionnent dans un environnement virtuel. La virtualisation est généralement associée à des pertes de performances pour la VM, car la couche intermédiaire de virtualisation elle-même nécessite des ressources. Les VM offrent de nombreux avantages par rapport à l'exécution d'un système d'exploitation ou d'une application directement sur un système physique. Les avantages les plus importants sont les suivants :


	1. Les applications et les services d'une VM n'interfèrent pas les uns avec les autres.
	2. Indépendance totale du système invité par rapport au système d'exploitation du système hôte et au matériel physique sous-jacent.
	3. Les VM peuvent être déplacées ou clonées sur d'autres systèmes par simple copie
	4. Les ressources matérielles peuvent être allouées dynamiquement par l'intermédiaire de l'hyperviseur.
	5. Utilisation plus efficace des ressources matérielles existantes
	6. Des temps de provisionnement plus courts pour les systèmes et les applications
	7. Gestion simplifiée des systèmes virtuels
	8. Disponibilité accrue des machines virtuelles grâce à l'indépendance par rapport aux ressources physiques

### Introduction à VirtualBox

Une excellente alternative gratuite à VMware Workstation est [VirtualBox](https://www.virtualbox.org/). Avec VirtualBox, les disques durs sont émulés dans des fichiers conteneurs, appelés Virtual Disk Images (**VDI**). Outre le format VDI, VirtualBox peut également gérer les fichiers de disque dur des produits de virtualisation VMware (**.vmdk**), le format **Virtual Hard Disk** (**.vhd**), et d'autres. Nous pouvons également convertir ces formats externes à l'aide de l'outil de ligne de commande VBoxManager qui fait partie de VirtualBox. Nous pouvons installer VirtualBox à partir de la ligne de commande ou télécharger le fichier d'installation à partir du [site web officiel](https://www.virtualbox.org/wiki/Downloads) et l'installer manuellement.

#### Installation de VirtualBox

Installation de VirtualBox

```session shell
tchadoyeon@htb[/htb]$ sudo apt install virtualbox virtualbox-ext-pack -y
```

![](https://academy.hackthebox.com/storage/modules/87/vbox.png)

A partir de l'exemple d'une VM créée montré ci-dessus, nous pouvons voir quelles sont les options de configuration disponibles pour la VDI dans VirtualBox. Nous avons également la possibilité et la fonction d'encrypter la VM, ce que nous devrions toujours utiliser. Nous utiliserons cette option dès que nous aurons préparé notre VM en conséquence et qu'elle sera prête à être utilisée.

## CONTENEURS

Un **conteneur** ne peut pas être défini comme une machine virtuelle, mais comme un groupe isolé de **processus** fonctionnant sur un seul hôte et correspondant à une application complète, y compris sa configuration et ses dépendances. Cette application est emballée dans un format précisément défini et réutilisable. Toutefois, contrairement à une VM classique sur VMware Workstation, un conteneur ne contient pas de système d'exploitation ni de noyau. Il ne s'agit donc pas d'un système d'exploitation virtualisé. C'est pourquoi les conteneurs sont nettement plus minces que les machines virtuelles conventionnelles. C'est précisément parce qu'il ne s'agit pas de véritables machines virtuelles que l'on parle également de virtualisation d'applications dans ce contexte.

Un problème important lors du déploiement de nouvelles applications ou de nouvelles versions est que chaque application dépend de certains aspects de son environnement. Il s'agit, par exemple, des paramètres locaux ou des bibliothèques de fonctions. Souvent, les paramètres de l'environnement de développement diffèrent de ceux de l'environnement de test et de production. Il peut alors rapidement arriver que, contrairement aux attentes, une application fonctionne différemment ou pas du tout en production.

|Machine virtuelle|**Container**|
|---|---|
|Elle peut contenir des applications et le système d'exploitation complet|**Elle peut contenir des applications et seulement les composants nécessaires du système d'exploitation, tels que les bibliothèques et les binaires.**|
|Un hyperviseur tel que VMware ESXi fournit la virtualisation|**Le système d'exploitation avec le moteur de conteneur fournit sa propre virtualisation**|
|Plusieurs machines virtuelles fonctionnent de manière isolée sur un serveur physique|**Plusieurs conteneurs fonctionnent de manière isolée sur un système d'exploitation.**|

#### Docker Installation

Docker Installation on Linux

```shell-session
tchadoyeon@htb[/htb]$ sudo apt update -y 
tchadoyeon@htb[/htb]$ sudo apt install docker.io -y
```

Docker Installation on Windows

```powershell-session
C:\> IEX((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
C:\> choco upgrade chocolatey
C:\> choco install docker-desktop
```

### Introduction à Vagrant

[Vagrant](https://www.vagrantup.com/) est un outil qui permet de créer, configurer et gérer des machines virtuelles ou des environnements de machines virtuelles. Les machines virtuelles ne sont pas créées et configurées manuellement mais sont décrites en code dans un **fichier Vagrant**. Pour mieux structurer le code du programme, le fichier Vagrant peut inclure des fichiers de code supplémentaires. Le code peut ensuite être traité à l'aide de l'interface de programmation Vagrant. De cette manière, nous pouvons créer, approvisionner et démarrer nos propres machines virtuelles. De plus, si les VM ne sont plus nécessaires, elles peuvent être détruites tout aussi rapidement et facilement. Vagrant propose d'emblée des fournisseurs pour VMware et Docker.

![](https://stefanscherer.github.io/content/images/2016/03/windows_swarm_demo.png)
#### Installation de Vagrant

Installation de Vagrant sur Linux

```session shell
tchadoyeon@htb[/htb]$ sudo apt update -y 
tchadoyeon@htb[/htb]$ sudo apt install virtualbox virtualbox-dkms vagrant
```

Installation de Vagrant sur Windows

```Session PowerShell
C:\N- IEX((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
C:\N- choco upgrade chocolatey
C:\N- cinst virtualbox cyg-get vagrant
```

## LINUX
### Installation d'un OS sur une VM

Since we want to encrypt our data and information on the VM using [Logical Volume Manager](https://en.wikipedia.org/wiki/Logical_volume_management) (**LVM**), we should select the "**Encrypt system**" option. It is also recommended to create a **Swap (no Hibernate)** for our system.

**LVM** is a partitioning scheme mainly used in Unix and Linux environments, which provides a level of abstraction between disks, partitions, and file systems. Using LVM, it is possible to form dynamically changeable partitions, which can also extend over several disks. After that we will be asked to specify our username, hostname and a password.

Once we have made all our required entries, we can confirm them and start configuring **LVM**.

### LUKS Encryption

**LVM** is an additional abstraction layer between physical data storage and the computer's operating system with its logical data storage area and the file system. LVM supports the organization of logical volumes in a RAID array to protect computers from individual hard disk failure. Unlike RAID, however, the LVM concept does not provide redundancy. It has been present in almost all Unix and Linux distributions but also for other operating systems. Windows or macOS also have the concept of LVM but use different names for it like [Storage Spaces](https://docs.microsoft.com/en-us/windows-server/storage/storage-spaces/overview) (Windows) or [CoreStorage](https://en.wikipedia.org/wiki/Core_Storage) (macOS).

Once we get to the partition disk step, we will be asked for an **encryption passphrase** for encryption and decryption. We should keep in mind that **this passphrase should be very strong**, and we should use a password manager of our choice to store it. We will then have to enter this passphrase again to make sure that we did not make a mistake when we first entered it.

After selecting the passphrase and confirming it, we will get an overview of all the partitions that have been created and configured. We will have other options available to us, as listed above. If we do not want to make any further configurations, we can now finish partitioning and let the changes be written accordingly.

Now the operating system's installation takes place, and as soon as this is finished, the VM is restarted. After the restart, we get a window that asks us for our passphrase to unlock the system.

If we have entered the passphrase correctly, then the operating system will boot up completely, and we will be able to log in. Here we enter the password for the username we have created.

### Updates & APT Package Manager

sources list

```shell-session
┌─[cry0l1t3@parrot]─[~]
└──╼ $ cat /etc/apt/sources.list.d/parrot.list

# parrot repository
# this file was automatically generated by parrot-mirror-selector
deb https://deb.parrot.sh/parrot/ rolling main contrib non-free
#deb-src https://deb.parrot.sh/parrot/ rolling main contrib non-free
deb https://deb.parrot.sh/parrot/ rolling-security main contrib non-free
#deb-src https://deb.parrot.sh/parrot/ rolling-security main contrib non-free
```

Here the package manager can access a list of HTTP and FTP servers and obtain and install the corresponding packages from there. If packages are searched for, they are automatically loaded from the list of available repositories. Since program versions can be compared quickly under APT and can be loaded automatically from the repositories list, updating existing program packages under APT is relatively easy and comfortable.

#### Updating OS

```shell-session
┌─[cry0l1t3@parrotos]─[~]
└──╼ $ sudo apt update -y && sudo apt full-upgrade -y && sudo apt autoremove -y && sudo apt autoclean -y
[sudo] password for cry0l1t3:                                                 

Hit:1 https://deb.parrot.sh/parrot rolling InRelease
Hit:2 https://deb.parrot.sh/parrot rolling-security InRelease
Reading package lists... Done
Building dependency tree
Reading state information... Done
2310 packages can be upgraded. Run 'apt list --upgradable' to see them.       
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Calculating upgrade... Done
The following packages were automatically installed and are no longer required:
  cryptsetup-nuke-password dwarfdump
  ...SNIP...
```

Then we can install the most necessary tools we need for our penetration tests. Here it is recommended to create a list of tools to simplify our automation process (note that many of these come pre-installed in distros such as Parrot).

#### Tools List

```shell-session
┌─[cry0l1t3@parrotos]─[~]
└──╼ $ cat tools.list

netcat
ncat
nmap
wireshark
tcpdump
hashcat
ffuf
gobuster
hydra
zaproxy
proxychains
sqlmap
radare2
metasploit-framework
python2.7
python3
spiderfoot
theharvester
remmina
xfreerdp
rdesktop
crackmapexec
exiftool
curl
seclists
testssl.sh
git
vim
tmux
```

If there are only a few packages that we want to install, we can enter them manually in the following command.

#### Installing Additional Tools

```shell-session
┌─[cry0l1t3@parrotos]─[~]
└──╼ $ sudo apt install netcat ncat nmap wireshark tcpdump ...SNIP... git vim tmux -y
[sudo] password for cry0l1t3:       

Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages were automatically installed and are no longer required: 
  libarmadillo9 libboost-locale1.71.0 libcfitsio8 libdap25 libgdal27 libgfapi0
  ...SNIP...
```

However, if the list contains more than just five packages, we should always create a list and keep it updated. With the following command, we will install all the tools from the list at once using APT.

#### Installing Additional Tools from a List

```shell-session
┌─[cry0l1t3@parrotos]─[~]
└──╼ $ sudo apt install $(cat tools.list | tr "\n" " ") -y
[sudo] password for cry0l1t3:       

Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages were automatically installed and are no longer required: 
  libarmadillo9 libboost-locale1.71.0 libcfitsio8 libdap25 libgdal27 libgfapi0
  ...SNIP...
```

### Using Github

We will also come across tools that are not found in the repositories and therefore have to download them manually from Github. For example, we are still missing specific tools for Privilege Escalation and want to download the [Privilege-Escalation-Awesome-Scripts-Suite](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite). We can do that using the following command:

#### Clone Github Repository

```shell-session
┌─[cry0l1t3@parrotos]─[~]
└──╼ $ git clone https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite.git

Cloning into 'privilege-escalation-awesome-scripts-suite'...
remote: Enumerating objects: 29, done.
remote: Counting objects: 100% (29/29), done.
remote: Compressing objects: 100% (17/17), done.
remote: Total 5242 (delta 18), reused 22 (delta 11), pack-reused 5213
Receiving objects: 100% (5242/5242), 18.65 MiB | 5.11 MiB/s, done.
Resolving deltas: 100% (3129/3129), done.
```

#### List Contents

```shell-session
┌─[cry0l1t3@parrotos]─[~]
└──╼ $ ls -l privilege-escalation-awesome-scripts-suite/

total 16
-rwxrwxr-x 1 cry0l1t3 cry0l1t3 1069 Mar 23 16:41 LICENSE
drwxrwxr-x 3 cry0l1t3 cry0l1t3 4096 Mar 23 16:41 linPEAS
-rwxrwxr-x 1 cry0l1t3 cry0l1t3 2506 Mar 23 16:41 README.md
drwxrwxr-x 4 cry0l1t3 cry0l1t3 4096 Mar 23 16:41 winPEAS
```

### Snapshot

After installing all known and relevant packages and repositories, it is highly recommended to take a **VM snapshot**. In the following steps, we will make changes to specific configuration files. If we are not careful, this can make parts of the system or even the entire system unusable. We do not have to repeat all our previous steps, and we should now create a snapshot and name it "**Initial Setup**".

Nevertheless, **before** we create this snapshot, we should shut down the OS. This will significantly reduce the time required to create the snapshot. Otherwise, the snapshot will be taken from a running system that we will return to when we return to it.

If we break the system in some way while performing subsequent configuration steps we can revert back to a good, known, working copy. A snapshot (powered off) should be taken after every major configuration stage. It is also a good idea to periodically take a VM snapshot during a penetration test in case something goes wrong.

#### Create a Snapshot

![](https://academy.hackthebox.com/storage/modules/87/vm_snapshot.png)

#### Completed Tasks

![](https://academy.hackthebox.com/storage/modules/87/vm_snapshot3.png)

### Terminal Adjustment

Now that we have created a snapshot and can work with our configurations, let us look at our terminal environment. Many different terminal emulators emulate the actual command line input we use to enter and execute the system's commands. The one that is the best for us depends on many personal desires and expectations. Here is a small list of very popular terminal emulators:

|[Terminator](https://terminator-gtk3.readthedocs.io/en/latest/)|[Guake](http://guake-project.org/)|[iTerm2](https://iterm2.com/)|[Terminology](https://www.enlightenment.org/docs/apps/terminology.md)|
|---|---|---|---|

A very efficient alternative, which can also be used as an extension, is [Tmux](https://github.com/tmux/tmux/wiki). **Tmux** is a terminal multiplexer that allows creating a whole shell session with multiple windows and subwindows from a single shell window. As we know, started processes abort when the terminal session or SSH connection disappears. Tmux's console keeps the process alive by working with sessions. For example, if we are connected to a constantly running server in this way, we can close the terminal or shut down the computer on the local client without terminating the Tmux session. If we log back into the remote server via SSH, we can view the existing sessions and rejoin the desired session.

[Ippsec](https://www.youtube.com/channel/UCa6eh7gCkpPo5XXUDfygQQA) has also created a short video where he introduced Tmux. There he explains some advantages of Tmux and shows with examples how he works with it. We will see his approach in all his videos.

#### Ippsec - Tmux

[Introduction to Tmux on Youtube](https://www.youtube.com/watch?v=Lqehvpe_djs&feature=youtu.be)

Another handy component that we should adapt to our needs is the Bash prompt. The [bashrcgenerator](http://bashrcgenerator.com/) makes it very easy for us to design our bash prompt the way we want it to be displayed. For our penetration tests, it is crucial to have the order of the given commands to configure our Bash prompt to display timestamps.

#### Customize Bash Prompt

```shell-session
┌─[cry0l1t3@parrotos]─[~]
└──╼ $ cp .bashrc .bashrc.bak
┌─[cry0l1t3@parrotos]─[~]
└──╼ $ echo 'export PS1="-[\[$(tput sgr0)\]\[\033[38;5;10m\]\d\[$(tput sgr0)\]-\[$(tput sgr0)\]\[\033[38;5;10m\]\t\[$(tput sgr0)\]]-[\[$(tput sgr0)\]\[\033[38;5;214m\]\u\[$(tput sgr0)\]@\[$(tput sgr0)\]\[\033[38;5;196m\]\h\[$(tput sgr0)\]]-\n-[\[$(tput sgr0)\]\[\033[38;5;33m\]\w\[$(tput sgr0)\]]\\$ \[$(tput sgr0)\]"' >> .bashrc
```

#### Customized Bash Prompt

```shell-session
-[Tue Mar 23-00:39:51]-[cry0l1t3@parrotos]-
-[~]$ 
```

Another advantage of this is that we can filter out our commands by the **minus** (**-**) at the beginning later in our logs and thus see a list with only the date, time, and command specified. There are countless variations on how we can also design the look and feel of our Linux distro. Apart from the terminal, we can also customize our desktop manager. There is even a [community](https://www.reddit.com/r/unixporn/) on Reddit which designs the GUIs in many different ways.

### Automation

The automation process is also an essential part of our preparation for penetration testing. Especially when it comes to internal penetration tests, where we have internet access and can adapt the workstation we are working on to our needs. This should be fast and efficient. For this, we have to create (in the best case) Bash scripts that automatically adjust our settings to the new system. Let us take the configuration and adjustment of our Bash prompt as an example. An example script can consist of the same commands we already configured.

#### Bash Prompt Customization Script - Prompt.sh

```bash
#!/bin/bash

#### Make a backup of the .bashrc file
cp ~/.bashrc ~/.bashrc.bak

#### Customize bash prompt
echo 'export PS1="-[\[$(tput sgr0)\]\[\033[38;5;10m\]\d\[$(tput sgr0)\]-\[$(tput sgr0)\]\[\033[38;5;10m\]\t\[$(tput sgr0)\]]-[\[$(tput sgr0)\]\[\033[38;5;214m\]\u\[$(tput sgr0)\]@\[$(tput sgr0)\]\[\033[38;5;196m\]\h\[$(tput sgr0)\]]-\n-[\[$(tput sgr0)\]\[\033[38;5;33m\]\w\[$(tput sgr0)\]]\\$ \[$(tput sgr0)\]"' >> ~/.bashrc
```

If we then host this script on our VPS, we can retrieve it from our customer's Linux workstation and apply it.

#### Request Prompt.sh

```shell-session
user@workstation:~$ curl -s http://myvps.vps-provider.net/Prompt.sh | bash
```

#### Customized Bash Prompt

```shell-session
-[Wed Mar 24-11:27:15]-[user@workstation]-
-[~]$ 
```

A simple designation of these scripts is also of great use. For example, suppose we assume that we want to configure our Bash prompt and other operating system components. In that case, we need to name the scripts for this as **understandably** as possible. If we have created several scripts like this, we can write a simple Bash script from memory on the working station, which then does all the configuration. Let us assume we have created the following list of scripts, and we are hosting these on our **Virtual Private Server** (**VPS**):

#### Customization Scripts

```shell-session
user@workstation:~$ cat customization-scripts.txt

Prompt.sh
Tools.sh
GUI.sh
Tmux.sh
Vim.sh
```

Now we could write a Bash script that takes care of all these settings for us or even combines them into a single command:

#### Customization Scripts Execution

```shell-session
user@workstation:~$ for script in $(cat customization-scripts.txt); do curl -s http://myvps.vps-provider.net/$script | bash; done
```

With this command, each customization script is retrieved and executed one by one from our VPS. This allows us to make any changes to the workstation or our new VM quickly from memory.

### Final Snapshot

Let us get back to our VM. Once we have adjusted all our configurations and settings, we should create a **Final snapshot** again to save our settings. We will want to add all the changes and tasks to the description of the new snapshot.

If we want to test our installation scripts to see if they work the way we want them to, we can copy them to our host system, revert our VM to the **Initial Setup** snapshot and run those scripts there. When we are satisfied with our scripts, we can then switch back to our **Final snapshot** and continue with VM encryption.

### VM Encryption

In addition to LVM encryption, we can encrypt the entire VM with another strong password. This gives us an extra layer of protection that will protect our results and any customer data residing on the system. This means that no one will be able to start the VM without the password we set.

Now that we have shut down and powered off the VM, we go to **Edit virtual machine settings** and select the **Options** tab.

There we will find more additional functions and settings that we can use later. Relevant for us now is the **Access Control** settings. Once we have encrypted it, we will not be able to create a clone of it without first decrypting it. More about this can be found in the VMware [documentation](https://docs.vmware.com/en/VMware-Workstation-Pro/16.0/com.vmware.ws.using.doc/GUID-8A64D0EF-CB0E-4C50-A034-3FD5C0A0F905.html).

When we close VMware and open it again, we are asked for the password for this VM. Once this has been entered correctly, we can then boot and work with it accordingly.

## WINDOWS

Windows computers serve an essential role as testbeds and victims for aspiring penetration testers like ourselves. However, it can make for a great penetration testing platform as well. There are some advantages to using Windows as our daily driver. It will blend in most enterprise environments so that we will appear physically and virtually less suspicious. It is easier to navigate and communicate with other hosts on an Active Directory domain if we use Windows versus Linux and some Python tooling. Traversing SMB and utilizing shares is much easier this way. With this in mind, it can be beneficial to familiarize ourselves with Windows and set a standard that ensures we have a stable and effective platform to perform our actions.

Building our penetration testing platform can help us in multiple ways:

1. Since we built it and installed only the tools necessary, we should have a better understanding of what is happening under the hood. This also allows us to ensure we do not have any unnecessary services running that could potentially be a risk to ourselves and the customer when on an engagement.

2. It provides us the flexibility of having multiple operating system types at our disposal if needed. These same systems used for our engagements can also serve as a testbed for payloads and exploits before launching them at the customer.

3. By building and testing the systems ourselves, we know they will function as intended during the penetration test and save ourselves time troubleshooting during the engagement.


With all this in mind, where do we start? Fortunately for us, there are many new features with Windows that were not available just a few years ago. **Windows Subsystem for Linux (WSL)** is an excellent example of this. It allows for Linux operating systems to run alongside our Windows install. This can help us by giving us a space to run tools developed for Linux right inside our Windows host without the need for a hypervisor program or installation of a third-party application such as VirtualBox or Docker.

This section will examine and install the core components we will need to get our systems in fighting shape, such as **WSL, Visual Studio Code, Python, Git, and the Chocolatey Package Manager**. Since we are utilizing this platform to perform penetration test functions, it will also require us to make changes to our host's security settings. Keep in mind, most exploitation tools and code are just that, **USED for EXPLOITATION** and can be harmful to your host if not careful. Be mindful of what we install and run. If we do not isolate these tools off, Windows Defender will almost certainly delete any detected files and applications it deems harmful, breaking our setup. OK, let us dive in.

---

### Installation Requirements

The installation of the Windows VM is done in the same way as the Linux VM. We can do this on a bare-metal host or in a hypervisor. With either option, we have some requirements to think about when installing Windows 10.

#### Hardware Requirements

	Processor that runs at 1GHz or greater. Dual-core or better is ideal.
	2G of RAM minimum, 4G or more is ideal.
	60G of Hard Drive space. This ensures there is room for the OS and some tools. Size can vary based on the number of tools we install on the host.
	Network connectivity, if possible, two network adapters.

Ideally, we have a moderate processor that can handle intensive loads at times. If we are attempting to run Windows virtualized, our host will need at least four cores to give two to the VM. Windows can get a bit beefy with updates and tool installs, so 80G of storage or more is ideal. When it comes to RAM, 4G would be a minimum to ensure we do not have any latency or issues while performing our penetration tests.

#### Software Requirements

Unlike most Linux distributions, Windows is a licensed product. To stay in good standing, ensure we are adhering to the terms of use. For now, a great place to start is to grab a copy of a Developer VM [here](https://developer.microsoft.com/en-us/windows/downloads/virtual-machines/). We can use this to begin building out our platform. The Developer Evaluation platform comes pre-configured with:

	Windows 10 Version 2004
	Windows 10 SDK Version 2004
	Visual Studio 2019 with the UWP, .NET desktop, and Azure workflows enabled and also includes the Windows Template Studio extension
	Visual Studio Code
	Windows Subsystem for Linux with Ubuntu installed
	Developer mode enabled

The VM comes pre-configured with a user: **IEUser** and Password **Passw0rd!**. It is a trial virtual machine, so it has an expiration date of 90 days. Keep this in mind when configuring it. Once we have a baseline VM, take a snapshot.

### Core Changes

To prepare our Windows host, we have to make a few changes before installing our fun tools:

1. We will need to update our host to ensure it is working at the required level and keep our security posture as strong as possible.

2. We will want to install the Windows Subsystem for Linux and the [Chocolatey Package manager](https://chocolatey.org/). Once these tasks are completed, we can make our exclusions to Windows Defender scanning policies to ensure they will not quarantine our newly installed tools and scripts. From this point, it is now time to install our tools and scripts of choice.

3. We will finish our buildout by taking a backup or snapshot of the host to have a fallback point if something happens to it.


### Updates

To keep with our command-line use, we will work at utilizing the command-line whenever possible. To start installing updates on our host, we will need the PSWindowsUpdate module. To acquire it, we will open an administrator Powershell window and issue the following commands:

#### Updates

```powershell-session
PS C:\htb> Get-ExecutionPolicy -List

Scope ExecutionPolicy
----- ---------------
MachinePolicy Undefined
UserPolicy Undefined
Process Undefined
CurrentUser Undefined
LocalMachine Undefined 
```

We must first check our systems Execution Policy to ensure we can download, load, and run modules and scripts. The above command will show us a list output with the policy set for each scope. In our case, we do not want this change to be permanent, so we will only change the ExecutionPolicy for the scope of **Process**.

#### Execution Policy

```powershell-session
PS C:\htb> Set-ExecutionPolicy Unrestricted -Scope Process

Execution Policy Change
The execution policy helps protect you from scripts that you do not trust. 
Changing the execution policy might expose you to the security risks described in the about_Execution_Policies help topic at https:/go.microsoft.com/fwlink/?LinkID=135170. Do you want to change the execution policy?
[Y] Yes [A] Yes to All [N] No [L] No to All [S] Suspend [?] Help (default is "N"): A

PS C:\htb> Get-ExecutionPolicy -List

Scope ExecutionPolicy
----- ---------------
MachinePolicy Undefined
UserPolicy Undefined
Process Unrestricted
CurrentUser Undefined
LocalMachine Undefined
```

Once we set our ExecutionPolicy, recheck it to make sure our change took effect. By changing the Process scope policy, we ensure our change is temporary and only applies to the current Powershell process. Changing it for any other scope will modify a registry setting and persist until we change it again.

Now that we have our ExecutionPolicy set, let us install the PSWindowsUpdate module and apply our updates. We can do so by:

#### PSWindowsUpdate

```powershell-session
PS C:\htb> Install-Module PSWindowsUpdate 

Untrusted repository 
You are installing the modules from an untrusted repository. If you trust this repository, 
change its InstallationPolicy value by running the Set-PSRepository cmdlet. 
Are you sure you want to install the modules from 'PSGallery'?
[Y] Yes [A] Yes to All [N] No [L] No to All [S] Suspend [?] Help (default is "N"): A 
```

Once the module installation completes, we can import it and run our updates.

```powershell-session
PS C:\htb> Import-Module PSWindowsUpdate 

PS C:\htb> Install-WindowsUpdate -AcceptAll

X ComputerName Result KB Size Title
- ------------ ------ -- ---- -----
1 DESKTOP-3... Accepted KB2267602 510MB Security Intelligence Update for Microsoft Defender Antivirus - KB2267602...
1 DESKTOP-3... Accepted 17MB VMware, Inc. - Display - 8.17.2.14
2 DESKTOP-3... Downloaded KB2267602 510MB Security Intelligence Update for Microsoft Defender Antivirus - KB2267602...
2 DESKTOP-3... Downloaded 17MB VMware, Inc. - Display - 8.17.2.14 3 DESKTOP-3... Installed KB2267602 510MB Security Intelligence Update for Microsoft Defender Antivirus - KB2267602... 3 DESKTOP-3... Installed 17MB VMware, Inc. - Display - 8.17.2.14 

PS C:\htb> Restart-Computer -Force

```

The above Powershell example will import the **PSWindowsUpdate** module, run the update installer, and then reboot the PC to apply changes. Be sure to run updates regularly, especially if we plan to use this host frequently and not destroy it at the end of each engagement. Now that we have our updates installed let us get our package manager and other essential core tools.

### Chocolatey Package Manager

Chocolatey is a free and open software package management solution that can manage the installation and dependencies for our software packages and scripts. It also allows for automation with Powershell, Ansible, and several other management solutions. Chocolatey will enable us to install the tools we need from one source instead of downloading and installing each tool individually from the internet. Follow the Powershell windows below to learn how to install Chocolatey and use it to gather and install our tools.

#### Chocolatey

```powershell-session
PS C:\htb> Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

Forcing web requests to allow TLS v1.2 (Required for requests to Chocolatey.org)
Getting latest version of the Chocolatey package for download.
Not using proxy.
Getting Chocolatey from https://community.chocolatey.org/api/v2/package/chocolatey/0.10.15.
Downloading https://community.chocolatey.org/api/v2/package/chocolatey/0.10.15 to C:\Users\DEMONS~1\AppData\Local\Temp\chocolatey\chocoInstall\chocolatey.zip
Not using proxy.
Extracting C:\Users\DEMONS~1\AppData\Local\Temp\chocolatey\chocoInstall\chocolatey.zip to C:\Users\DEMONS~1\AppData\Local\Temp\chocolatey\chocoInstall
Installing Chocolatey on the local machine
Creating ChocolateyInstall as an environment variable (targeting 'Machine')
Setting ChocolateyInstall to 'C:\ProgramData\chocolatey'

...SNIP...

Chocolatey (choco.exe) is now ready.
You can call choco from the command-line or PowerShell by typing choco.
Run choco /? for a list of functions.
You may need to shut down and restart powershell and/or consoles
first prior to using choco.
Ensuring Chocolatey commands are on the path
Ensuring chocolatey.nupkg is in the lib folder

```

We have now installed chocolatey. The Powershell string we issued sets our ExecutionPolicy for the session and then downloads the installer from chocolatey.org and runs the script. Next, we will update chocolatey then start installing packages. To ensure no issues arise, it is recommended that we periodically restart our host.

```powershell-session
PS C:\htb> choco upgrade chocolatey -y 

Chocolatey v0.10.15
Upgrading the following packages:
chocolatey
By upgrading, you accept licenses for the packages.
chocolatey v0.10.15 is the latest version available based on your source(s).

Chocolatey upgraded 0/1 packages.
See the log for details (C:\ProgramData\chocolatey\logs\chocolatey.log).
```

Now that we are sure chocolatey is up-to-date let us run our packages. We can use **choco** to install packages by issuing the **choco install pkg1 pkg2 pkg3** command listing out the package you need one by one separated by spaces. Alternatively, we can use a **packages.config** file for the installation. This is an XML file formatted so that chocolatey can install a list of packages. One helpful command to use is **choco info pkg**. It will show us various information about a package if it is available in the choco repository. See the [install page](https://docs.chocolatey.org/en-us/choco/commands/install) for more info on how to utilize chocolatey.

```powershell-session
PS C:\htb> choco info vscode 

Chocolatey v0.10.15
vscode 1.55.1 [Approved]
Title: Visual Studio Code | Published: 4/9/2021
Package approved as a trusted package on Apr 09 2021 01:34:23.
Package testing status: Passing on Apr 09 2021 00:49:32.
Number of Downloads: 1999367 | Downloads for this version: 19751
Package url
Chocolatey Package Source: https://github.com/chocolatey-community/chocolatey-coreteampackages/tree/master/automatic/vscode
Package Checksum: 'fTzzpEG+cspu7FUdqMbj8EqaD8cRIQ/cXtAUv7JGVB9uc23vuGNiuceqM94irt+nx8MGM0xAcBwdwBH+iE+tgQ==' (SHA512)
Tags: microsoft visualstudiocode vscode development editor ide javascript typescript admin foss cross-platform
Software Site: https://code.visualstudio.com/
Software License: https://code.visualstudio.com/License
Software Source: https://github.com/Microsoft/vscode
Documentation: https://code.visualstudio.com/docs
Issues: https://github.com/Microsoft/vscode/issues
Summary: Visual Studio Code
Description: Build and debug modern web and cloud applications. Code is free and available on your favorite platform - Linux, Mac OSX, and Windows.
...SNIP...
```

Above is an example of using the info option with chocolatey.

```powershell-session
PS C:\htb> choco install python vscode git wsl2 openssh openvpn 

Chocolatey v0.10.15
Installing the following packages:
python;vscode;git;wsl2;openssh;openvpn 
...SNIP... 

Chocolatey installed 20/20 packages.
See the log for details (C:\ProgramData\chocolatey\logs\chocolatey.log).

Installed:
- kb2919355 v1.0.20160915
- python v3.9.4
- kb3033929 v1.0.5
- chocolatey-core.extension v1.3.5.1
- kb2999226 v1.0.20181019
- python3 v3.9.4
- openssh v8.0.0.1
- vcredist2015 v14.0.24215.20170201
- gpg4win-vanilla v2.3.4.20191021
- vscode.install v1.55.1
- wsl2 v2.0.0.20210122
- kb2919442 v1.0.20160915
- openvpn v2.4.7
- git.install v2.31.1
- vscode v1.55.1
- vcredist140 v14.28.29913
- kb3035131 v1.0.3
- dotnet4.5.2 v4.5.2.20140902
- git v2.31.1
- chocolatey-windowsupdate.extension v1.0.4

PS C:\htb> RefreshEnv 
```

We can see in the terminal above that **choco** installed the packages we requested and pulled any dependencies required. Issuing the **RefreshEnv** command will update Powershell and any environment variables that were applied. Up to this point, we have our core tools installed. These tools will enable our operations. To install other packages, use the **choco install pkg** command to pull any operational tools we need. We have included a list of helpful packages that can aid us in completing a penetration test below. See the automation section further down to begin automating installing the tools and packages we commonly need and use.

---

### Windows Terminal

Windows Terminal is Microsoft's updated release for a GUI terminal emulator. It supports using many different command-line tools to include Command Prompt, PowerShell, and Windows Subsystem for Linux. The terminal allows for the use of customizable themes, configurations, command-line arguments, and custom actions. A terminal is a versatile tool for managing multiple shell types and will quickly become a staple for most.

![image](https://academy.hackthebox.com/storage/modules/87/terminal-example.png)

To install Terminal with Chocolatey:

```powershell-session
PS C:\htb> choco install microsoft-windows-terminal

Chocolatey v0.10.15
2 validations performed. 1 success(es), 1 warning(s), and 0 error(s).

Validation Warnings:
- A pending system reboot request has been detected, however, this is
being ignored due to the current Chocolatey configuration. If you
want to halt when this occurs, then either set the global feature
using:
choco feature enable -name=exitOnRebootDetected
or pass the option --exit-when-reboot-detected.

Installing the following packages:
microsoft-windows-terminal
By installing you accept licenses for the packages.
Progress: Downloading microsoft-windows-terminal 1.6.10571.0... 100%

microsoft-windows-terminal v1.6.10571.0 [Approved]
microsoft-windows-terminal package files install completed. Performing other installation steps.
Progress: 100% - Processing The install of microsoft-windows-terminal was successful.
Software install location not explicitly set, could be in package or
default install location if installer.

Chocolatey installed 1/1 packages.
See the log for details (C:\ProgramData\chocolatey\logs\chocolatey.log). 
```

### Windows Subsystem for Linux 2

**Windows Subsystem for Linux 2** (**WSL2**) is the second iteration of Microsoft's architecture that allows users to run Linux instances, provides the ability to run Bash scripts and other apps like Vim, Python, etc. WSL also allows us to interact with the Windows operating system and file structure from a Unix instance. Best of all, it is done without the use of a hypervisor like VirtualBox or Hyper-V.

What does this mean for us? Having the ability to interact and utilize Linux native tools and applications from our Windows host provides us with a hybrid environment and the flexibility that comes with it. To install the subsystem, the quickest route is to utilize chocolatey.

#### Chocolatey - WSL2

```powershell-session
PS C:\htb> choco install WSL2

Chocolatey v0.10.15
2 validations performed. 1 success(es), 1 warning(s), and 0 error(s).
Installing the following packages:
wsl2
By installing you accept licenses for the packages.
Progress: Downloading wsl2 2.0.0.20210122... 100%

wsl2 v2.0.0.20210122 [Approved]
wsl2 package files install completed. Performing other installation steps.
...SNIP...
wsl2 may be able to be automatically uninstalled.
The install of wsl2 was successful.
Software installed as 'msi', install location is likely default.

Chocolatey installed 1/1 packages.
See the log for details (C:\ProgramData\chocolatey\logs\chocolatey.log).
```

Once WSL is installed, we can add the Linux platform of our choice. The most common one to find is Ubuntu on the Microsoft store. Current Linux distributions supported for WSL are:

- [Ubuntu 16.04 LTS](https://www.microsoft.com/store/apps/9pjn388hp8c9), [18.04 LTS](https://www.microsoft.com/store/apps/9N9TNGVNDL3Q), and [20.04 LTS](https://www.microsoft.com/store/apps/9n6svws3rx71)
- [openSUSE Leap 15.1](https://www.microsoft.com/store/apps/9NJFZK00FGKV)
- [SUSE Linux Enterprise Server 12 SP5](https://www.microsoft.com/store/apps/9MZ3D1TRP8T1) and [15 SP1](https://www.microsoft.com/store/apps/9PN498VPMF3Z)
- [Kali Linux](https://www.microsoft.com/store/apps/9PKR34TNCV07)
- [Debian GNU/Linux](https://www.microsoft.com/store/apps/9MSVKQC78PK6)
- [Fedora Remix for WSL](https://www.microsoft.com/store/apps/9n6gdm4k2hnc)
- [Pengwin](https://www.microsoft.com/store/apps/9NV1GV1PXZ6P) and [Pengwin Enterprise](https://www.microsoft.com/store/apps/9N8LP0X93VCP)
- [Alpine WSL](https://www.microsoft.com/store/apps/9p804crf0395)

To install the distribution of our choice, just click the link above, and it will take us to the Microsoft Store page for the distro. Once we have it installed, we need to open a PowerShell prompt and type **bash**.

![image](https://academy.hackthebox.com/storage/modules/87/windows-bash.png)

From this point, we can use it as a regular OS, alongside our Windows install.

### Security Configurations and Defender Modifications

Since we will be using this platform as a penetration testing host, we may run into some issues with Windows Defender finding our tools unsavory. Windows Defender will scan and quarantine or remove anything it deems potentially harmful. To make sure Defender does not mess up our plans, we will add some exclusion rules to ensure our tools stay in place.

#### Windows Defender Exemptions for the Tools' Folders.

- **C:\Users\your user here\AppData\Local\Temp\chocolatey\**

- **C:\Users\your user here\Documents\git-repos\**

- **C:\Users\your user here\Documents\scripts\**


These three folders are just a start. As we add more tools and scripts, we may need to add more exclusions. To exclude these files, we will run a PowerShell command.

#### Adding Exclusions

```powershell-session
PS C:\htb> Add-MpPreference -ExclusionPath "C:\Users\your user here\AppData\Local\Temp\chocolatey\"
```

Repeat the same steps for each folder we wish to exclude.

### Tool Install Automation

Utilizing Chocolatey for package management makes it super easy to automate the initial install of core tools and applications. We can use a simple PowerShell script to pull everything for us in one run. Here is an example of a simple script to install some of our requirements. As usual, before executing any scripts, we need to change the execution policy. Once we have our initial script built, we can modify it as our toolkit changes and reuse it to speed up our setup process.

#### Choco Build Script

```powershell
# Choco build script

write-host "*** Initial app install for core tools and packages. ***"

write-host "*** Configuring chocolatey ***"
choco feature enable -n allowGlobalConfirmation

write-host "*** Beginning install, go grab a coffee. ***"
choco upgrade wsl2 python git vscode openssh openvpn netcat nmap wireshark burp-suite-free-edition heidisql sysinternals putty golang neo4j-community openjdk

write-host "*** Build complete, restoring GlobalConfirmation policy. ***"
choco feature disable -n allowGlobalCOnfirmation
```

When scripting with Chocolatey, the developers recommend a few rules to follow:

- always use **choco** or **choco.exe** as the command in your scripts. **cup** or **cinst** tends to misbehave when used in a script.

- when utilizing options like **-n** it is recommended that we use the extended option like **--name**.

- Do not use **--force** in scripts. It overrides Chocolatey's behavior.


Not all of our packages can be acquired from Chocolatey. Fortunately for us, a majority of what is left resides in Github. We can set up a script for this and download the repositories and binaries we need, then extract them to our scripts folder. Below we will build out a quick example of a Git script. First, let us see what it looks like to clone a repository to our local host.

#### Git Clone

```powershell-session
PS C:\htb> git clone https://github.com/dafthack/DomainPasswordSpray.git

Cloning into 'DomainPasswordSpray'...
remote: Enumerating objects: 149, done.
remote: Counting objects: 100% (6/6), done.
remote: Compressing objects: 100% (5/5), done.
Receiving objects:  94% (141/149)(delta 1), reused 5 (delta 1), pack-reused 143
Receiving objects: 100% (149/149), 51.70 KiB | 3.69 MiB/s, done.
Resolving deltas: 100% (52/52), done.
PS C:\Users\demonstrator\Documents\scripts> ls


    Directory: C:\Users\demonstrator\Documents\scripts


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         4/16/2021   4:58 PM                DomainPasswordSpray


PS C:\Users\demonstrator\Documents\scripts> cd .\DomainPasswordSpray\
PS C:\Users\demonstrator\Documents\scripts\DomainPasswordSpray> ls


    Directory: C:\Users\demonstrator\Documents\scripts\DomainPasswordSpray


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
-a----         4/16/2021   4:58 PM          19419 DomainPasswordSpray.ps1
-a----         4/16/2021   4:58 PM           1086 LICENSE
-a----         4/16/2021   4:58 PM           2678 README.md
```

We issued the **git clone** command with the URL to the repository we needed. From the output, we can tell it created a new folder in our scripts folder then populated it with the files from the GitHub repository.

### Testing VMs

It is common and very relevant to prepare our penetration testing VMs and VMs for the most common operating systems and their patch levels. This is especially necessary if we want to mirror our target machines and test our exploits before applying them to real machines. For example, we can install a Windows 10 VM that is built on different [patches](https://support.microsoft.com/en-us/topic/windows-10-update-history-24ea91f4-36e7-d8fd-0ddb-d79d9d0cdbda) and [releases](https://docs.microsoft.com/en-us/windows/release-health/release-information). This will save us considerable time in the course of our penetration tests to configure them again. These VMs will help us test our approach and exploits to understand better how the interconnected system might react to them because it may be that we will only have one attempt to execute the exploit.

The good thing here is that we do not have to set up 20 VMs for this but can work with snapshots. For example, we can start with Windows 10 version 1607 (OS build 14393) and update our system step by step and create a snapshot of the clean system from each of these updates and patches. Updates and patches can be downloaded from the [Microsoft Update Catalog](https://www.catalog.update.microsoft.com/Search.aspx?q=KB4550994). We just need to use the **Kb article** designation, and there we will find the appropriate files to download and patch our systems.

Tools that can be used to install older versions of Windows:

- [Chocolatey](https://community.chocolatey.org/)

- [MediaCreationTool.bat](https://gist.github.com/AveYo/c74dc774a8fb81a332b5d65613187b15)

- [Microsoft Windows and Office ISO Download Tool](https://www.heidoc.net/joomla/technology-science/microsoft/67-microsoft-windows-and-office-iso-download-tool%EF%BB%BF)

- [Rufus](https://rufus.ie/en_US/)

## VPS PROVIDERS

Un **Serveur Privé Virtuel** (**VPS**) est un environnement isolé créé sur un serveur physique en utilisant la technologie de virtualisation. Un VPS fait fondamentalement partie des solutions **Infrastructure-as-a-Service** (**IaaS**). Cette solution offre tous les avantages d'un serveur ordinaire, avec des ressources spécialement allouées et une liberté de gestion totale. Nous pouvons choisir librement le système d'exploitation et les applications que nous voulons utiliser, sans aucune restriction de configuration. Cette VM utilise les ressources d'un serveur physique et offre aux utilisateurs diverses fonctionnalités de serveur comparables à celles d'un serveur dédié. C'est pourquoi on l'appelle aussi **serveur virtuel dédié** (VVDS).

Un VPS se positionne comme un compromis entre l'hébergement partagé bon marché et la location généralement coûteuse de la technologie des serveurs dédiés. L'idée de ce modèle d'hébergement est d'offrir aux utilisateurs la gamme de fonctions la plus complète possible à des prix raisonnables. La réplication virtuelle de systèmes informatiques individuels sur un système hôte standard implique beaucoup moins d'efforts pour un hébergeur web que la mise à disposition de composants matériels distincts pour chaque client. L'encapsulation permet d'obtenir une grande indépendance des différents systèmes invités. Chaque SPV sur la base matérielle partagée est isolé des autres systèmes d'exploitation parallèles. Dans la plupart des cas, le VPS est utilisé dans les buts suivants, mais pas seulement :

| | | | |
|---|---|---|---|
|Serveur web|LAMP/XAMPP stack|Serveur de messagerie|Serveur DNS|
|Serveur de développement|Serveur proxy|Serveur de test|Dépot de code|
|Pentesting|VPN|Serveur de jeux|Serveur C2|

Nous pouvons choisir parmi une gamme de systèmes d'exploitation Windows et Linux pour fournir l'environnement d'exploitation requis pour l'application ou le logiciel souhaité lors de l'installation.

|Provider|Price|RAM|CPU Cores|Storage|Transfer/Bandwidth|
|---|---|---|---|---|---|
|Vultr](https://www.vultr.com/products/cloud-compute/)|$10/mois|2GB|1 CPU|55 GB|2 TB|
|Linode](https://www.linode.com/pricing/)|10$/mois|2GB|1 CPU|50 GB|2 TB|
|DigitalOcean](https://www.digitalocean.com/pricing)|$10/mo|2GB|1 CPU|50 GB|2 TB|
|OneHostCloud](https://onehostcloud.hosting)|14.99$/mois|2GB|1 CPU|50 GB|2 TB|

## VPS SETUP

Whether we are on an internal or external penetration test, a VPS that we can use is of great importance to us in many different cases. We can store all our resources on it and access them from almost any point with internet access. Apart from the fact that we first have to set up the VPS, we also have to prepare the corresponding folder structure and its services. In this example, we will deal with the provider called [Vultr](https://www.vultr.com/) and set up our VPS there. For the other providers, the configuration options look almost identical.

We should still read the individual information about their components carefully and understand what kind of VPS we will be working with in the future. In Vultr, we can choose one of the four different servers. For our purposes, the **Cloud Computer** server is sufficient for now.

![](https://academy.hackthebox.com/storage/modules/87/vps1.png)

Next, we need to select the location closest to us. This will ensure an excellent connection to our server. If we need to perform a penetration test on another continent, we can also set up a VPS there using our automation scripts and perform the penetration test from there.

![](https://academy.hackthebox.com/storage/modules/87/vps2.png)

For the server type, we select which operating system should be installed on the VPS. ParrotOS is based on **Debian**, just like Ubuntu. Here we can choose one of these two or go to advanced options and upload our ISO.

![](https://academy.hackthebox.com/storage/modules/87/vps2_2.png)

If we want to upload our ISO, we have to do it via a public link to that ISO. We can go to the [ParrotOS website](https://www.parrotsec.org/security-edition/), copy the link to the **mirror server** for the appropriate version, and paste it into Vultr.

![](https://academy.hackthebox.com/storage/modules/87/vps3.png)

Next, we need to choose the performance level for our VPS. This is one of the points where the cost can change a lot. For our purposes, however, we can choose one of the two options on the left. However, here we need to select the performance for which we want to use the VPS. If the VPS is to be used for advanced purposes and provides many requests and services, 1024MB memory will not be enough. Therefore, it is always advisable to first set up the installation of our OS locally in a VM and then check the services' load.

![](https://academy.hackthebox.com/storage/modules/87/vps4.png)

Next, we have the choice if we want to use [IPv6](https://en.wikipedia.org/wiki/IPv6) for our VPS. This is highly recommended because many firewalls are only protected against IPv4, and **IPv6** is forgotten. We can also allow **automatic backups**, **private networking**, and **DDOS protection**, but at a higher cost.

![](https://academy.hackthebox.com/storage/modules/87/vps5.png)

After that, we can generate our SSH keys, which we can use to log in to the VPS via SSH later. We can also generate these keys later on our VPS or our VM or own host operating system. Let's use our VM to generate the key pair.

#### Generate SSH Keys

```shell-session
┌─[cry0l1t3@parrot]─[~]
└──╼ $ ssh-keygen -t rsa -b 4096 -f vps-ssh

Generating public/private rsa key pair.
Enter passphrase (empty for no passphrase): ******************
Enter same passphrase again: ******************
Your identification has been saved in vps-ssh
Your public key has been saved in vps-ssh.pub
The key fingerprint is:
SHA256:zXyVAWK00000000000000000000VS4a/f0000+ag cry0l1t3@parrot
The key's randomart image is:
...SNIP...
```

With the command shown above, we generate two different keys. The **vps-ssh** is the **private key** and must not be shared anywhere or with anyone. The second **vps-ssh.pub** is the **public key** which we can now insert in the Vultr control panel.

#### SSH Keys

```shell-session
┌─[cry0l1t3@parrot]─[~]
└──╼ $ ls -l vps*

-rw------- 1 cry0l1t3 cry0l1t3 3434 Mar 30 12:23 vps-ssh
-rw-r--r-- 1 cry0l1t3 cry0l1t3  741 Mar 30 12:23 vps-ssh.pub
```

![](https://academy.hackthebox.com/storage/modules/87/vps6.png)

Finally, we choose a hostname and the server label with which we want to name our VPS.

![](https://academy.hackthebox.com/storage/modules/87/vps7.png)

Once the VPS is installed, we can access it via SSH.

#### SSH Using Password

```shell-session
tchadoyeon@htb[/htb]$ ssh root@<vps-ip-address>

root@<vps-ip-address>'s password: 

[root@VPS ~]# 
```

After that, we should add a new user for the VPS to not run our services with root or administrator privileges. For this, we can then generate another SSH key and insert it for this user.

#### Adding a New Sudo User

```shell-session
[root@VPS ~]# adduser cry0l1t3
[root@VPS ~]# usermod -aG sudo cry0l1t3
[root@VPS ~]# su - cry0l1t3
Password: 

[cry0l1t3@VPS ~]$
```

#### Adding Public SSH Key to VPS

```shell-session
[cry0l1t3@VPS ~]$ mkdir ~/.ssh
[cry0l1t3@VPS ~]$ echo '<vps-ssh.pub>' > ~/.ssh/authorized_keys
[cry0l1t3@VPS ~]$ chmod 600 ~/.ssh/authorized_keys
```

Once we have added this to the **authorized_keys** file, we can use the **private key** to log in to the system via SSH.

#### Using SSH Keys

Using SSH Keys

```shell-session
tchadoyeon@htb[/htb]$ ssh cry0l1t3@<vps-ip-address> -i vps-ssh

[cry0l1t3@VPS ~]$ 
```

## VPS HARDENING

Another necessary step in our configuration and setup of our VPS is the hardening of the system and access. We should limit our access to the VPS to SSH and disable all other services on the VPS. Finally, we will reduce the attack vectors to a minimum and provide only one possible access to our VPS, which we will secure in the best possible way. We should keep in mind that, if possible, we should not store any sensitive data on the VPS, or at least only for a short period when we perform an internal penetration test. In doing so, we should follow the principle that someone could gain access to the system sooner or later.

However, since in this case, the VPS is only used as a source for our organization and tools and we can access these resources via SSH, we should secure and harden the SSH server accordingly so that no one else (or at least no one other than the team members) can access it. There are many ways to harden it, and this includes the following precautions, but not limited to:

- Install Fail2ban
- Working only with SSH keys
- Reduce Idle timeout interval
- Disable passwords
- Disable x11 forwarding
- Use a different port
- Limit users' SSH access
- Disable root logins
- Use SSH proto 2
- Enable 2FA Authentication for SSH

**It is highly recommended to try these settings and precautions first in a local VM we have created before making these settings on a VPS.**

One of the first steps in hardening our system is updating and bringing the system **up-to-date**. We can do this with the following commands:

#### Update the System

```shell-session
[cry0l1t3@VPS ~]$ sudo apt update -y && sudo apt full-upgrade -y && sudo apt autoremove -y && sudo apt autoclean -y
```

## SSH Hardening

SSH is always installed on the VPS, giving us guaranteed access to the server in advance. Now we can change some of the settings in the configuration file **/etc/ssh/sshd_config** to enforce these security measures for our SSH server. In this file, we will comment out, change or add some lines. The entire list of possible settings that can be made for the SSH daemon can be found on the [man page](https://man.openbsd.org/sshd_config).

#### Install Fail2Ban

```shell-session
[cry0l1t3@VPS ~]$ sudo apt install fail2ban -y
```

Once we have installed it, we can find the configuration file at **/etc/fail2ban/jail.conf**. We should make a backup if we make a mistake somewhere and it does not work as it should.

#### Fail2Ban Config Backup

```shell-session
[cry0l1t3@VPS ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.conf.bak
```

In this file, we will find the field commented out with **# [sshd]**. In most cases, we will find the first line commented out there. Otherwise, we add this and two more, as shown in the following example:

#### /etc/fail2ban/jail.conf

```shell-session
...SNIP...

# [sshd]
enabled = true
bantime = 4w
maxretry = 3
```

With this, we enable monitoring for the SSH server, set the **ban time** to four weeks, and allow a maximum of 3 **attempts**. The advantage of this is that once we have configured our **2FA** feature for SSH, fail2ban will ban the IP address that has entered the **verification code** incorrectly three times too. We should make the following configurations in the **/etc/ssh/sshd_config** file:

#### Editing OpenSSH Config

```shell-session
[cry0l1t3@VPS ~]$ sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config.bak
[cry0l1t3@VPS ~]$ sudo vim /etc/ssh/sshd_config
```

|Settings|Description|
|---|---|
|LogLevel VERBOSE|Gives the verbosity level that is used when logging messages from SSH daemon.|
|PermitRootLogin no|Specifies whether root can log in using SSH.|
|MaxAuthTries 3|Specifies the maximum number of authentication attempts permitted per connection.|
|MaxSessions 5|Specifies the maximum number of open shell, login, or subsystem (e.g., SFTP) sessions allowed per network connection.|
|HostbasedAuthentication no|Specifies whether rhosts or /etc/hosts.equiv authentication together with successful public key client host authentication is allowed (host-based authentication).|
|PermitEmptyPasswords no|When password authentication is allowed, it specifies whether the server allows login to accounts with empty password strings.|
|ChallengeResponseAuthentication yes|Specifies whether challenge-response authentication is allowed.|
|UsePAM yes|Specifies if PAM modules should be used for authentification.|
|X11Forwarding no|Specifies whether X11 forwarding is permitted.|
|PrintMotd no|Specifies whether SSH daemon should print /etc/motd when a user logs in interactively.|
|ClientAliveInterval 600|Sets a timeout interval in seconds, after which if no data has been received from the client, the SSH daemon will send a message through the encrypted channel to request a response from the client.|
|ClientAliveCountMax 0|Sets the number of client alive messages which may be sent without SSH daemon receiving any messages back from the client.|
|AllowUsers (username)|This keyword can be followed by a list of user name patterns, separated by spaces. If specified, login is allowed only for user names that match one of the patterns.|
|Protocol 2|Specifies the usage of the newer protocol which is more secure.|
|AuthenticationMethods publickey,keyboard-interactive|Specifies the authentication methods that must be successfully completed for a user to be granted access.|
|PasswordAuthentication no|Specifies whether password authentication is allowed.|

## 2FA Authentication

With the configuration shown above, we have already taken essential steps to harden our SSH. Therefore, we can now go one step further and configure **2-factor authentication** (**2FA**). With this, we use a third-party software called Google Authenticator, which generates a six-digit code every 30 seconds that is needed to authenticate our identity. These six-digit codes represent a so-called **One-Time-Password** (OTP). **2FA** has proven itself an authentication method, not least because of its relatively high-security standard compared to the time required for implementation. Two different and independent authentication factors verify the identity of a person requesting access. We can find more information about **2FA** [here](https://en.wikipedia.org/wiki/Multi-factor_authentication).

We will use Google Authenticator as our authentication application on our Android or iOS smartphone. For this, we need to download and install the application from the Google/Apple Store. A guide on setting up Google Authenticator on our smartphone can be found [here](https://support.google.com/accounts/answer/185839?co=GENIE.Platform%3DDesktop&hl=en). To configure 2FA with Google Authenticator on our VPS, we need the [Google-Authenticator PAM module](https://github.com/google/google-authenticator-libpam). We can then install it and execute it to start configuring it as follows:

#### Installing Google-Authenticator PAM Module

```shell-session
[cry0l1t3@VPS ~]$ sudo apt install libpam-google-authenticator -y
[cry0l1t3@VPS ~]$ google-authenticator

Do you want authentication tokens to be time-based (y/n) y

Warning: pasting the following URL into your browser exposes the OTP secret to Google:
  https://www.google.com/chart?chs=200x200&chld=M|0&cht=qr&chl=otpauth://totp/cry0l1t3@parrot%3Fsecret%...SNIP...%26issuer%3Dparrot

   [ ---- QR Code ---- ]

Your new secret key is: ***************
Enter code from app (-1 to skip):
```

If we follow these steps, then a **QR code** and a **secret key** will appear in our terminal, which we can then scan with the Google Authenticator app or enter the secret key there. Once we have scanned the QR code or entered the secret key, we will see the first **OTP** (six-digit number) on our smartphone. We enter this in our terminal to synchronize and authorize Google Authenticator on our smartphone and our VPS with Google.

The module will then generate several **emergency scratch codes** (**backup codes**), which we should save safely. These will be used in case we lose our smartphone. Should this happen, we can then log in with the **backup codes**.

#### Google-Authenticator Configuration

```shell-session
Enter code from app (-1 to skip): <Google-Auth Code>

Code confirmed
Your emergency scratch codes are:
  21323478
  43822347
  60232018
  73234726
  45456791
  
Do you want me to update your "/home/cry0l1t3/.google_authenticator" file? (y/n) y

Do you want to disallow multiple uses of the same authentication
token? This restricts you to one login about every 30s, but it increases
your chances to notice or even prevent man-in-the-middle attacks (y/n) y

By default, a new token is generated every 30 seconds by the mobile app.
In order to compensate for possible time-skew between the client and the server,
we allow an extra token before and after the current time. This allows for a
time skew of up to 30 seconds between authentication server and client. If you
experience problems with poor time synchronization, you can increase the window
from its default size of 3 permitted codes (one previous code, the current
code, the next code) to 17 permitted codes (the 8 previous codes, the current
code, and the 8 next codes). This will permit for a time skew of up to 4 minutes
between client and server.
Do you want to do so? (y/n) n

If the computer that you are logging into isn't hardened against brute-force
login attempts, you can enable rate-limiting for the authentication module.
By default, this limits attackers to no more than 3 login attempts every 30s.
Do you want to enable rate-limiting? (y/n) y
```

Next, we need to configure the [PAM](https://en.wikipedia.org/wiki/Linux_PAM) module for the SSH daemon. To do this, we first create a backup of the file and open the file with a text editor such as Vim.

#### 2FA PAM Configuration

```shell-session
[cry0l1t3@VPS ~]$ sudo cp /etc/pam.d/sshd /etc/pam.d/sshd.bak
[cry0l1t3@VPS ~]$ sudo vim /etc/pam.d/sshd
```

We comment out the "**@include common-auth**" line by putting a "**#**" in front of it. Besides, we add two new lines at the end of the file, as follows:

#### /etc/pam.d/sshd

```shell-session
#@include common-auth

...SNIP...

auth required pam_google_authenticator.so
auth required pam_permit.so
```

Next, we need to adjust our settings in our SSH daemon to allow this authentication method. In this configuration file (**/etc/ssh/sshd_config**), we need to add two new lines at the end of the file as follows:

#### /etc/ssh/sshd_config

```shell-session
...SNIP...

AuthenticationMethods publickey,keyboard-interactive
PasswordAuthentication no
```

Finally, we have to restart the SSH server to apply the new configurations and settings.

#### Restart SSH Server

```shell-session
[cry0l1t3@VPS ~]$ sudo service ssh restart
```

Now we can test this and try to login to the SSH server with our SSH key and check if everything works as intended.

#### 2FA SSH Login

```shell-session
tchadoyeon@htb[/htb]$ ssh cry0l1t3@VPS -i ~/.ssh/vps-ssh

Enter passphrase for key 'cry0l1t3': *************
Verification code: <Google-Auth Code>
```

Finally, we can transfer all our resources, scripts, notes, and other components to the VPS using [SCP](https://en.wikipedia.org/wiki/Secure_copy_protocol).

#### SCP Syntax

```shell-session
tchadoyeon@htb[/htb]$ scp -i <ssh-private-key> -r <directory to transfer> <username>@<IP/FQDN>:<path>
```

#### Resources Transfer

```shell-session
tchadoyeon@htb[/htb]$ scp -i ~/.ssh/vps-ssh -r ~/Pentesting cry0l1t3@VPS:~/

Enter passphrase for key 'cry0l1t3': *************
Verification code: <Google-Auth Code>
```

### Closing Thoughts

We should now have an understanding of using common virtualization platforms such as VMWare Workstation/Player and VirtualBox and be comfortable with setting up VMs from scratch. After practicing the examples in this Module, we should also be comfortable setting up and hardening both a Windows and Linux attack machine for our penetration testing purposes. Finally, it is worth replicating the steps for standing up a VPS using a provider such as Vultr and practicing hardening it based on the steps in this section. It is important for us to be able to configure, harden, and maintain the systems that we use during our assessments.
