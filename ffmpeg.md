selectionne plusieurs morceaux audio ( -af = audio filter, aselect = audio select )
`ffmpeg -i ma_video.opus -af "aselect='between(t,5,10)+between(t,15,20)+between(t,25,30)', asetpts=N/SR/TB" out.opus`

extrait un morceau de la piste et en fait une copie
`ffmpeg -i 01_02_2020.mp3 -ss 00:22:22 -to 00:27:02 -c copy 008.mp3`

concaténer les pistes audio en une (les pistes sont listées dans un fichier `.txt`)
`ffmpeg -f concat -safe 0 -i my_list_of_audio_to_concat.txt -c copy frout.mp3`
format du fichier :
```
file "my_list_1.mp3"
file "my_list_2.mp3"
file "my_list_3.mp3"
```

concat tous les fichier mp3 dans le dossier courant en un fichier frout.mp3
`ffmpeg -f concat -safe 0 -i <(for f in ./*.mp3; do echo "file '$PWD/$f'"; done) -c copy frout.mp3`

récupère toutes les images d'une vidéo
`ffmpeg -i video.avi -r 1 -f image2 -q 1 %05d.jpg`

connaitre la durée d'un fichier audio
`ffmpeg -i [emplacement du fichier] 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,// > outfile`

slow down speed of mp4 by 2
`ffmpeg -i femme.mp4 -filter:v "setpts=2.0*PTS" output.mp4`

speed up speed of mp4 by 2
`ffmpeg -i femme.mp4 -filter:v "setpts=0.5*PTS" output.mp4`
