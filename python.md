
Les parenthèses ( ) délimitent les tuples, les crochets [ ] délimitent les listes et les accolades { } délimitent les dictionnaires.

installer pytest pour python3
`sudo apt-get install -y python3-pytest`

installer package pour python3
par default la commande pip installe des packets sous python2
pour installer sous python3 faire :

`python3.6 -m pip install [nom du packet]`

Lancer un test
`pytest test_essai.py`

converti en bit
`return b'{}'`

transférer des fichier d'une machine vers une autre
`python3 -m http.server`

---
créer un environnement virtuel
`python3 -m venv <env_name>`

activer l'environnement virtuel
`source <env_name>/bin/activate`

désactiver
`deactivate`

---
créer un environnement virtuel avec `pip`
`pipenv`

activer l'env
`pipenv shell`

désactiver
`exit`

installer un paquet
`pipenv install <paquet>`
or
`pipenv install <paquet> --dev`

avec un ficher. exemple : `requirements.txt`
```
django==4.2
pillow==5.2.1
...
```
`pipenv install -r ./requirements.txt`

désinstaller un paquet
`pipenv uninstall <paquet>`

---
pour savoir d'où vient un import
```python
import sys
sys.executable
```