#commandes

lancer le serveur
`symfony serve`

list toutes les commandes de la console
`symfony console`

check route
`symfony console debug:router`

donne le détail de la route
`symfony console debug:router <ma_route>`

vérifie la route et le controller qui corresponde au chemin
`symfony console router:match </ma_route>`
