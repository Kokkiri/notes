
install rust
`curl --proto '=https' --tlsv1.3 https://sh.rustup.rs -sSf | sh`

check version
`rustc --version`

compile program ( where main.rs is the main file of the program )
`rustc main.rs`

get info about some error
`rustc --explain <code error>`

---
###### CARGO
description: package installer

create a project
`cargo new <project_name>`

build a project
`cargo build`

build and run a project in one step
`cargo run`

build a project without producing a binary to check for errors
`cargo check`

```
Instead of saving the result of the build in the same directory as our code, Cargo stores it in the target/debug directory.
```

#### gestion des erreurs

```rust
let uncertain_result = op_risky();
if let Err(error) = uncertain_result {
	// code
}
```

