
Created mardi 24 juillet 2018

### Installation sous Manjaro

mise à jour
`sudo pacman -Syu`

installation
`sudo pacman -S docker`

check version
`docker version`

get info
`docker info`

---

La différence entre `docker run` et `docker exec` est que `docker exec` exécute une commande sur un conteneur en cours d'exécution. En revanche, `docker run` crée un conteneur temporaire, exécute la commande dans celui-ci et arrête le conteneur lorsqu'il a terminé.

---

### Commande docker

Lancer le docker deamon
`systemctl start docker.service`

lance automatiquement le docker deamon à chaque démarrage de la machine.
`systemctl enable docker.service`

affiche tous les processus docker ( les services ) en cours ( -a liste tous les process lancer ou arrêté. -q liste tous les process par leur id )
`docker ps -a`

liste les images docker
`docker image ls`
`docker images`

stoper les proocess
`docker stop $(docker ps -aq)`

supprimer les containers
`docker rm $(docker ps -aq)`
`docker container prune`

supprime le réseaux docker
`docker network prune`

supprime les volumes
`docker volume prune`

supprime une images
`docker image rm <images_name>`

supprime les images `dangling` ( images qui ne sont pas versionner par un tag ) ( -a supprimer toutes les images locales )
`docker image prune -a`

Pour supprimer :
- tous les conteneurs qui ne sont pas en cours d'exécution
- tous les réseaux qui ne sont pas utilisés par au moins un conteneur en cours d'exécution
- toutes les images dangling qui ne sont pas taguées et qui ne sont pas utilisées par au moins un conteneur en cours d'exécution
- tous les caches utilisés pour la création d'images Docker.
( -a pour supprimer les images taguées )
`docker system prune -a`

relance un container
`sudo docker exec -it 6e1f9f9dbdfe bash`

liste les containers ( -a pour afficher tous les containers actif et inactif )
`sudo docker container ls -a`

mettre un container en pause
`docker pause <id or name>`

libérer la pause d'un container
`docker unpause <id or name>`

éxécuter une commande dans un container depuis sa machine
`docker exec <id or name> <cmd> <arg>`

---