
remplace tout les mots-clé dans un fichier
`sed -i 's/old-text/new-text/g' input.txt`

ajoute des guillemets autour des phrases
`sed 's/^/`/;s/$/`/' toto.md`

ajoute des back-tick sur toutes les phrases à l'exception des phrases qui comment par "# " par "- " et les lignes vident ( ! = exclusion | = separateur d'expression ^ = début de phrase $ = fin de phrase () = grouper les expressions )
`sed -i '/\(# \|- \|^$\)/! s/^/`/; /\(# \|- \|^$\)/! s/$/`/' Docker.md`