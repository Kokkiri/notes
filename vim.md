###### SHORTCUT

| shortcut | description |
|---|---|
| `i` | insertion |
| `v` | visual |
| `C v` | visual block |
| `M i` | insertion sur selection |
| `escape` | quitter mode insertion |
| `:x` | sauvergarder quitter |
| `:q` | quitter sans sauvegarder |
| `:q!` | forcer à quitter |
| `dd` | supprimer une ligne |
| `u`	| annuler |
| `x`	| supprimer une lettre |
| `5x` | supprime les 5 lettres suivant |
| `d5w` | supprime les 5 mots suivant |
| `y`	| copier |
| `yy` | copie la ligne |
| `p` | paste |
| `d0` | supprime toute la ligne depuis le debut jusqu'au curseur |
| `d$` | supprime toute la ligne depuis le curseur jusqu'à la fin |
| `d4 <Left-Arrow> <Right-Arrow>` | supprime 4 character à gauche ou à droite |
| `r <lettre>` | remplace un caractère |
| `C w` | switch between windows |
| `<num>gg` or `<num>G` | aller à la ligne n |
| `alt gr + k` | mettre en majuscule ou minuscule |
| `#` or `gd` | rechercher les occurences du mot survoler par le curseur |
| `:noh` or `ctrl l` | annuler une recherche |

---
##### insert on multiline
1. `C v` 
2. `M i`
3. insert your text
4. `escape escape`

https://doc.ubuntu-fr.org/vim

---
##### install plugins with vim-plug

paste command in `plugin.vim` files
`Plug 'neoclide/coc.nvim', {'branch': 'release'}`

then in your vim prompt type
`:PlugInstall`

---
##### COC ( Conquer Of Completion )

installation
`:CocInstall coc-json coc-tsserver coc-snippets coc-rust-analyzer`

list snippets
`:CocList snippets`

create `snippets` folder in your `nvim` folder
then create `python.snippets` file for exemple ( it will automaticaly recognise the snippets as python snippets ) 

content exemple of `python.snippets`
```json
snippet mlimport
import nympy as np
import panda
import sys
endsnippet
```

`mlimport` is the name of my snippet

autocomplete when Enter key is pressed ( in `init.vim` file )
```js
inoremap <silent><expr> <cr> coc#pum#visible() ? coc#_select_confirm() : "\<C-g>u\<CR>"
```

https://github.com/neoclide/coc.nvim

---
###### SET FOLDER AS PARENT DIRECTORY
in tree: go to selected directory
`<M-c>`

###### REPLACEMENT METHODS

Change each 'foo' to 'bar' in the current line.
`:s/foo/bar/g`

Change each 'foo' to 'bar' in all the lines.
`:%s/foo/bar/g`

Change each 'foo' to 'bar' in all the lines and ask confirmation.
`:%s/foo/bar/gc`

Change each 'foo' to 'bar' for all lines from line 5 to line 12 (inclusive).
`:5,12s/foo/bar/g`

Change each 'foo' to 'bar' for all lines from mark a to mark b inclusive (see Note below).
`:'a,'bs/foo/bar/g`

When compiled with +visual, change each 'foo' to 'bar' for all lines within a visual selection. Vim automatically appends the visual selection range ('<,'>) for any ex command when you select an area and enter :. Also, see Note below.
`:'<,'>s/foo/bar/g`

Change each 'foo' to 'bar' for all lines from the current line (.) to the last line ($) inclusive.
`:.,$s/foo/bar/g`

Change each 'foo' to 'bar' for the current line (.) and the two next lines (+2).
`:.,+2s/foo/bar/g`

Change each 'foo' to 'bar' in each line starting with 'baz'. 
`:g/^baz/s/foo/bar/g`

---
###### COMMAND LINE

Connaître la description d'un raccourci clavier
`verbose map <shortcut>`

###### CORRECT INDENT (this is not autoindent)
(`=`, the indent command can take motions. So, `gg` to get the start of the file, `=` to indent, `G` to the end of the file)
`gg=G`.

###### RELOCATE TAB
`:tabm <num>` or `:tabm -1` or `:tabm +1`

###### COPY LINE FROM VIM TO ANOTHER PROGRAM ( v mod ) 
`"+y`
