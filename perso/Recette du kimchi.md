#perso 

# RECETTE DE KIMCHI

**ingrédient :**

- choux chinois * 1
- gros sel
- poudre de piment rouge (Kotchoudjang)
- farine de riz
- 20 cl d’eau
- sucre roux
- radi blanc 1/2
- carotte * 1
- oignon blanc
- oignon rouge * 1 si t’as pas d’oignon blanc sinon 1/2
- ciboulette
- pomme fuji * 1
- ail 5 gousses environ
- gingembre
- des mini crevettes (Séoudjeot) si tu trouve pas c’est pas grave, moi même je sais pas où c’est.
- sauce poisson
- graine de sésame

**protocole :**

tailler le cul du choux en croix, écarter les parties de tel sorte que les feuilles à la tête se démèle.
Glisser du gros sel entre chaques feuilles et laisser saumurer au frigo pendant 5h minimum.
Quand c’est fait, lui couper la partie du cul restante parce que c’est dur et c’est pas bon.
Moi je coupe tout en morceau c’est plus facile à mélanger après.

**Couper les légumes :**

l’ail : prendre une gousse la mettre sous le plat d’un couteau à viande et taper dessus parcequ’elle le mérite.
Ça peau va élcater et c’est vachement plus facile à enlever. Faire pareil avec toutes les autres.
Ensuite hacher les gousses en petit morceau et mettre dans un bol.
Raper le gingembre et le mettre dans le bol.
Couper la ciboulette en petit morceau et la mettre dans le bol.
Couper les oignon blanc et rouge en petit morceau et mettre dans le bol.
Nettoyer les carottes et les couper en diagonale, recouper les tranches en fines lamelles, ça fait comme des carottes raper. Les mettre dans une assiette.
Couper le radi blanc en tranche et recouper les tranches en fines lamelles. Les mettre dans l’assiette.
La pomme c’est le dernier truc qu’on coupe parceque ça pourri vite à l’air libre. Couper en fines tranches puis en fines lamelles et mettre dans l’assiette.

**La sauce :**

verser les 20 cl d’eau dans une casserole puis mettre deux bonnes cuiellère à soupe de farine de riz. Mélanger jusqu’à ce que l’eau devienne blanche et homogène.
Commencer à faire chauffer à feu moyen et continuer de battre l’eau avec un fouet. Continuer jusqu’à ce que l’eau devienne pâte. Arrêter le feu.
Mettre du sucre roux dans la casserole.
Mettre de la poudre de piment rouge dans la casserole, la quantité dépend de tes goûts.
Cinq bonne cueillères à soupe, c’est bien mais tu peux mettre plus si tu veux.
Battre la pâte avec un fouet à main jusqu’à ce qu’elle devienne homogène.
Prendre un petit bol de mini crevette que tu rinces bien et que tu haches. Les mettre dans la sauce.
Mettre le bolle d’oignon, ail, gingembre, ciboulette dans la souce. Ça fera déjà un premier mélange.

**Tout mélanger :**

Prendre une bassine si t’en a parce que ça fait un paquet de bouffe ou alors un sac poubelle, c’est moins pratique mais ça marche aussi.
Tu mets toutes tes assiettes de légumes dans la bassine puis tu verses la sauce par dessus même si elle est encore chaude et là tu brasses avec tes mains.
Rajouter la sauce poisson, environ 5 cl ça devrait être bon. Elle sert à saler le kimchi.
Rajouter des graines de sésames.
Mettre dans des tupperwares ou je ne sais où et mettre dans le frigo.
Voilà.