#perso

# les symboles et leur noms

() parenthèses
\[\] crochets
{} acolades
'' single quotes
"" double quotes
\`\` backtick ou backquote
/  slash
\  anti-slash
~  tild
&  esperluette
%  modulo
\-  dash
_  underscore
<> chevrons
|  pipe

# déclaration de variables

let
const
var

var est façon de faire obslète, elle n'est pas recommandé
const pour les variables qui sont voués à ne pas être modifier
let pour toutes les autres

# la portée (scope) des variables

    let un = 1

    function my_function() {
        let deux = 2
        console.log("inside my_function", un, deux)
    }

    console.log("outside my_function", un, deux)

expected output :

> inside my_function 1 2
> deux is undefined

un est une variable global (avec une portée global)
deux est une variable local (avec une portée local)
je ne peux donc pas accéder à la variable deux depuis l'extérieur de la fonction

# les opérateurs

voici une liste non-exhaustive

= affectation
== égalité simple
=== égalite stricte (vérifie également que le type des deux variables est égale)
+= incrémentation
-= décrémentation
< inférieur à
<= inférieur ou égale à
&& "et" : vérifie que deux conditions sont exacte
|| "ou" : vérifie que l'une des deux condition est exacte

# les types

integer > 0
string > ""
array > \[\]
object > {}
boolean > false

    let tab = ["toto", 123, ["retoto"], {"nom": "momo"}]
    let obj = {
        "nom": "Zoro",
        "age": 40
        "films": ["Akira", "Ghost in the shell"],
        "autre_object": {
            "enfant": "Marie"
            "age": 17
        }
    }

    console.log(tab[1])
expected output
> 123

    console.log(tab[2][0])
expected output
> retoto

    console.log(obj.nom)
expected output
> Zoro

    console.log(obj["nom"])
expected output
> Zoro

    console.log(obj.films[1])
expected output
> Ghost in the shell

    console.log(obj.autre_object.enfant)
expected output
> Marie

# récupérer des balises avec JavaScript

    document.getElementById('mon_id')
permet de récupérer un élément en particulier

    document.getElementsByClassName('ma_classe')
permet de récupérer une collection de class

    document.getElementsByClassName('ma_classe')[0]
récupère le premier élément avec la class 'ma_classe'

    document.querySelector('p')
permet de récupérer la première balise p de notre html

    document.querySelectorAll('p')
permet de récupèrer tous les éléments de type paragraphe

    document.querySelectorAll('p')[1]
permet de récupèrer le deuxième élément de type paragraphe

# template litéral

s'utilise avec les backticks ``

    let nom = 'momo'
    console.log(`Hello ${nom}`)
expected output
> Hello momo