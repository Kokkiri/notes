#perso

# Local Storage

Les cookies et le stockage local ont des objectifs différents. Les cookies sont principalement destinés à être lus par le serveur, tandis que le stockage local ne peut être lu que par le client. La question est donc la suivante : dans votre application, qui a besoin de ces données - le client ou le serveur ?

Si c'est le client (votre JavaScript), alors il faut absolument changer. Vous gaspillez de la bande passante en envoyant toutes les données dans chaque en-tête HTTP.

Si c'est votre serveur, le stockage local n'est pas si utile parce que vous devez transmettre les données d'une manière ou d'une autre (avec Ajax ou des champs de formulaire cachés ou autre). Cela peut convenir si le serveur n'a besoin que d'un petit sous-ensemble des données totales pour chaque requête.

Dans tous les cas, vous devrez laisser votre cookie de session en tant que cookie.

En ce qui concerne la différence technique, et aussi ma compréhension :

- En dehors du fait qu'il s'agit d'une ancienne façon de sauvegarder des données, les cookies vous donnent une limite de 4096 octets (4095, en fait) - c'est par cookie. Le stockage local peut atteindre 5 Mo par domaine - la question SO le mentionne également.

- localStorage est une implémentation de l'interface de stockage. Il stocke des données sans date d'expiration, et n'est effacé que par JavaScript, ou en vidant le cache du navigateur / Données stockées localement - contrairement à l'expiration des cookies.

## Problems:

Web Storage (localStorage/sessionStorage) is accessible through JavaScript on the same domain. This means that any JavaScript running on your site will have access to web storage, and because of this can be vulnerable to cross-site scripting (XSS) attacks. XSS in a nutshell is a type of vulnerability where an attacker can inject JavaScript that will run on your page. Basic XSS attacks attempt to inject JavaScript through form inputs, where the attacker puts alert('You are Hacked'); into a form to see if it is run by the browser and can be viewed by other users.

## Prevention:

To prevent XSS, the common response is to escape and encode all untrusted data. But this is far from the full story. In 2015, modern web apps use JavaScript hosted on CDNs or outside infrastructure. Modern web apps include 3rd party JavaScript libraries for A/B testing, funnel/market analysis, and ads. We use package managers like Bower to import other peoples’ code into our apps.

What if only one of the scripts you use is compromised? Malicious JavaScript can be embedded on the page, and Web Storage is compromised. These types of XSS attacks can get everyone’s Web Storage that visits your site, without their knowledge. This is probably why a bunch of organizations advise not to store anything of value or trust any information in web storage. This includes session identifiers and tokens.

As a storage mechanism, Web Storage does not enforce any secure standards during transfer. Whoever reads Web Storage and uses it must do their due diligence to ensure they always send the JWT over HTTPS and never HTTP.

# Cookies

## Problems:

Cookies, when used with the HttpOnly cookie flag, are not accessible through JavaScript, and are immune to XSS. You can also set the Secure cookie flag to guarantee the cookie is only sent over HTTPS. This is one of the main reasons that cookies have been leveraged in the past to store tokens or session data. Modern developers are hesitant to use cookies because they traditionally required state to be stored on the server, thus breaking RESTful best practices. Cookies as a storage mechanism do not require state to be stored on the server if you are storing a JWT in the cookie. This is because the JWT encapsulates everything the server needs to serve the request.

However, cookies are vulnerable to a different type of attack: cross-site request forgery (CSRF). A CSRF attack is a type of attack that occurs when a malicious web site, email, or blog causes a user’s web browser to perform an unwanted action on a trusted site on which the user is currently authenticated. This is an exploit of how the browser handles cookies. A cookie can only be sent to the domains in which it is allowed. By default, this is the domain that originally set the cookie. The cookie will be sent for a request regardless of whether you are on galaxies.com or hahagonnahackyou.com.

## Prevention:

Modern browsers support the SameSite flag, in addition to HttpOnly and Secure. The purpose of this flag is to prevent the cookie from being transmitted in cross-site requests, preventing many kinds of CSRF attack.

For browsers that do not support SameSite, CSRF can be prevented by using synchronized token patterns. This sounds complicated, but all modern web frameworks have support for this.

For example, AngularJS has a solution to validate that the cookie is accessible by only your domain. Straight from AngularJS docs:

When performing XHR requests, the $http service reads a token from a cookie (by default, XSRF-TOKEN) and sets it as an HTTP header (X-XSRF-TOKEN). Since only JavaScript that runs on your domain can read the cookie, your server can be assured that the XHR came from JavaScript running on your domain. You can make this CSRF protection stateless by including a xsrfToken JWT claim:

    {
      "iss": "http://galaxies.com",
      "exp": 1300819380,
      "scopes": ["explorer", "solar-harvester", "seller"],
      "sub": "tom@andromeda.com",
      "xsrfToken": "d9b9714c-7ac0-42e0-8696-2dae95dbc33e"
    }

Leveraging your web app framework’s CSRF protection makes cookies rock solid for storing a JWT. CSRF can also be partially prevented by checking the HTTP Referer and Origin header from your API. CSRF attacks will have Referer and Origin headers that are unrelated to your application.

The full article can be found here: https://stormpath.com/blog/where-to-store-your-jwts-cookies-vs-html5-web-storage/

They also have a helpful article on how to best design and implement JWTs, with regards to the structure of the token itself: https://stormpath.com/blog/jwt-the-right-way/


With localStorage, web applications can store data locally within the user's browser. Before HTML5, application data had to be stored in cookies, included in every server request. Large amounts of data can be stored locally, without affecting website performance. Although localStorage is more modern, there are some pros and cons to both techniques.

# Cookies

## Pros

- Legacy support (it's been around forever)
- Persistent data
- Expiration dates
- Cookies can be marked as HTTPOnly which might limit XSS atacks to user browser during his - sesion (does not guarantee full immunity to XSS atacks).

## Cons

Each domain stores all its cookies in a single string, which can make parsing data difficult
Data is unencrypted, which becomes an issue because... ... though small in size, cookies are sent with every HTTP request Limited size (4KB)

# Local storage

## Pros

- Support by most modern browsers
- Persistent data that is stored directly in the browser
- Same-origin rules apply to local storage data
- Is not sent with every HTTP request
- ~5MB storage per domain (that's 5120KB)

## Cons

- Not supported by anything before: IE 8, Firefox 3.5, Safari 4, Chrome 4, Opera 10.5, iOS 2.0, Android 2.0
- If the server needs stored client information you purposely have to send it.

localStorage usage is almost identical with the session one. They have pretty much exact methods, so switching from session to localStorage is really child's play. However, if stored data is really crucial for your application, you will probably use cookies as a backup in case localStorage is not available. If you want to check browser support for localStorage, all you have to do is run this simple script:

        /* 
        * function body that test if storage is available
        * returns true if localStorage is available and false if it's not
        */
        function lsTest(){
            var test = 'test';
            try {
                localStorage.setItem(test, test);
                localStorage.removeItem(test);
                return true;
            } catch(e) {
                return false;
            }
        }

        /* 
        * execute Test and run our custom script 
        */
        if(lsTest()) {
            // window.sessionStorage.setItem(name, 1); // session and storage methods are very similar
            window.localStorage.setItem(name, 1);
            console.log('localStorage where used'); // log
        } else {
            document.cookie="name=1; expires=Mon, 28 Mar 2016 12:00:00 UTC";
            console.log('Cookie where used'); // log
        }


# Cookies:

- Introduced prior to HTML5.
- Has expiration date.
- Cleared by JS or by Clear Browsing Data of browser or after expiration date.
- Will sent to the server per each request.
- The capacity is 4KB.
- Only strings are able to store in cookies.
- There are two types of cookies: persistent and session.

# Local Storage:

- Introduced with HTML5.
- Does not have expiration date.
- Cleared by JS or by Clear Browsing Data of the browser.
- You can select when the data must be sent to the server.
- The capacity is 5MB.
- Data is stored indefinitely, and must be a string.
- Only have one type.
