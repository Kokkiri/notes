###### aptitude
liste les paquets installés

----
###### bat
alternative à `cat` avec syntaxe colorée

---
###### exa
un `ls` coloré

---
###### cmus
lecteur audio pour terminal

---
###### duf
un `df` graphique

---
###### Filius
simulation de réseaux

---
###### Gigolo
est un outil qui permet de rendre accessible les serveurs distant à mes outils locaux.
éxemple : modifier le code de zephir2 sur le serveur root@zephir2 avec atom

---
###### Gobuster
outil pour craquer des mots de passe avec une liste de mot de passe

---
###### GNS3
simulation de réseaux ( avancée )

---
###### Hexchat
chat

---
###### Hydra
outil pour craquer des mots de passe avec une liste de mot de passe

---
###### Jinja2
templates python ( définie la forme des messages retournés, des fichiers généré )

---
###### Jp2a
conversion d'image en ASCII

---
###### Katyusha
modélisation de base de donnée ( modèle merise )

----
###### LinPeas
outil pour monter en privilège

---
###### Mapascii
affiche la carte du monde dans le terminal
`telnet mapscii.me`

---
###### Mermaid
bibliothèque Javascript pour faire des diagrammes

---
###### Obsidian
équivalent zim mais non libre

---
###### Revolt
alternative libre à Discord

---
###### Screenfetch
affiche les infos principales conternant la machine

---
###### Shotcut
montage vidéo

---
###### tldr
alternative à `man` version courte

---
###### youtube-dl
téléchargement de piste vidéo

---
###### border-only
un theme XFCE sans bordure
https://www.xfce-look.org/p/1016214/

---
###### xclip
copie dans le presse papier en ligne de commande.

---
###### fontforge
pour créer des fonts